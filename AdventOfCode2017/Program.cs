﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode2017
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Day10();
        }

        private static void Day10()
        {
            var input = "14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244";

            //input = "AoC 2017";

            //var lengths = input.Split(',').Select(x => int.Parse(x)).ToArray();
            var lengths = input.ToCharArray().Concat(new int[] { 17, 31, 73, 47, 23 }.Select(x => (char)x)).ToList();

            var list = Enumerable.Range(0, 256).ToList();
            var currentPosition = 0;
            var skipSize = 0;

            void Round()
            {
                foreach (var length in lengths)
                {
                    var reversedSelection = Enumerable.Range(0, length)
                        .Select(x => CircularGet(list, currentPosition, x))
                        .Reverse()
                        .ToList();
                    for (int i = 0; i < length; i++)
                    {
                        CircularSet(list, currentPosition, i, reversedSelection[i]);
                    }
                    currentPosition = (currentPosition + length + skipSize) % list.Count;
                    skipSize++;
                }
            }

            for (int i = 0; i < 64; i++)
            {
                Round();
            }

            var denseHash = from i in Enumerable.Range(0, 16)
                            let block = list.Skip(i * 16).Take(16)
                            let xored = block.Aggregate((a, b) => a ^ b)
                            select xored;

            var hexDenseHash = string.Concat(denseHash.Select(b => b.ToString("x2")).ToArray());
        }

        private static T CircularGet<T>(IReadOnlyList<T> list, int baseIndex, int index)
            => list[(baseIndex + index) % list.Count];
        private static void CircularSet<T>(IList<T> list, int baseIndex, int index, T value)
          => list[(baseIndex + index) % list.Count] = value;

        private static void Day9()
        {
            var input = "{{{},{{{{<e\"\"a!!io<\"!!!!\"!'!!\"u!>},<\"o!!}i>,<!>!i<!!!!oi!>},<>},{},{{<!>,<{!u!!!>,<!!'<!>!,,!!\"!e'o}>}}},{},{<!!!!\"a\"<!!eo,!>}!!!>,io!>'!o>}},{{{<!!!'ou!!!>!oe!>!<!!i>,{<!>},<uoue>}},{},{{<}}>,<\"'',!!}{!!i{>}}},{{{{<'o!>>,<!>!{u}<e!>a<au!!o!>,<a!!\"\"a!!!>\">},{{<i{!>,<\"!>e!!'!!!!!>},<'o!!!,!e<o<>},{{}}}},{{<e'!!!!!!i\"a!>,<,>},{<'!!!><!}!!{!>},<,\"u>}}},{{{{<a{o!!!!!>!><<!!!>},<>}},{<!!\"!>e>,{<!>},<\"}a\"''!!!!!!!>o<}!!,>}}},{{{<!,}!\"}!!!>,<'!!!!e!>},<!>},<{!>},<,,!!}}!!u>},{{}}},{{{<i!>,<!><>}},{{}}},{{}}},{{{<>},<!>},<eu'!>},<u!''!!!>!!!>!!<>},{<>}},{{},{<!<<!!e!!!>,<>,{}},{{},{{<>},{{<!>!>!!!!a!>,<!!e}e!>{>}}}}}},{{{},{}}}}},{{{<a}a>}},{<!>,<!!!!'!>,<!!!!'o\"a!!eea\"!>\"e!u>,{{{<!!!!{ua>}}}},{{<}!!<,e>},<<!>,<'{!{ei!!!>>}}}},{{{{{<'!a>,<!!i<o!,!!\"<i!>,<!>},<o!!e}!!e<!!!>a>},{{<!!}!>,<!i}o!!i!>\"!>},<!>},<e'!!<!>!>,<{!>,<>,{}},{{},{{}}},{<!!u'!>,<}!!!!!>!!!>!>},<!>\"!!!>!!ui'>}}},{{<u!{<!!u{{!>},<!>},<a!!!!!!,!!!>},<\"{>}},{{},{{{<ei!!!{\"!>{!a<!><}<!>},<!!e>,{<!!!>'!!a<!!!>},<u}o<aa'!>,<>}},{<\",>}},{<!!!>{!!}!e!>,<<{!>},<<!>,<!<!>},<>,{<'!e!>},<!>,<i!!!!!>!>!>!'>}}},{{<<!>!!{>},{{<!oa{>},{<!>i!{>}}},{{{{{{{<!}ou<,!>,<!!a!>},<\">,<,}i!!!>!}!>!!!!!>},<e\"i!>io,!>},<!!!>}!!i>}}},{{},{<!!!!!!>,{{},{}}}},{<!>,<>}}},{{{<,!>i!!!>},<!>!{ue!!i>},{{{<,!>!>},<}!!{u'!}!>!!!>!>},<>}},{<u!u!!<<{!!<,}e{ua'>}}},{<!>!!!>!!!!!>!io>}},{{{{<e,iu\"!>,<!!!>!>e!{'>,{<'e!!'!,u!>},<u>}},{{<o'!!!>,<!>,<!!!>o!>a<u!>},<ao!!a!o},>}}}},{{{{}},{{<!!'!>},<!!!>},<!!aei<,!>>},{<!!!>!!!>\"}!oae>},{{<\"!!!<!!!>!>,<'!!!>a,<\"!>},<o!>>}}},{{{<!>,<'uo{!{>}},<}!!}<!!!!<e}u!!!>'a!>},<>}},{{{<!!i!!!!{<!!!!oo!!a}'!!!>!!'!!>},<!!!>,{oi{!!,!e!!!<>},{{},{}},{{<e<<!>,<!>,<!!!>},<}\"{e<!euue\"!\"o>},{<e!!!!!>oa>}}}},{{{<!>,<'<!!!>eo!>,<!aa},>,<'\"!!!!!>!!e\"!!!>'}!!>},{{{<\"!>},<{a!!'!!!>!!\"!'a!!!!!>!!u>},{<'>}}},{{<a!>i!>},<!'e!>!>!!!><e!>!>},<\">},{<<>}}},{{<<u!i}!>,<!!!>>},{}},{{<ao'u!>},<'u{',uo>,<!!a'{ue,>},{}}},{{<!>!!e!>,<e>,{{<!>},<{'!>,<!!!>'a,!!!>!'!>,<<!>!!!<!>,<<{\">}}},{<!>},<!}oii!o\"!>,<>}}},{{<<!>,<}!\"!>!>},<!!!>u>}}},{{<ei!!>}},{{{<!>>,{<>}},{{<!,oa}!!!>o{>},{}},{}},{{<\">}},{<o!!!>\"uaou!>,\"'>,{<!>},<<o\",!!!>!>u!e!>,<ui!>},<<>,<,!}}}\"!!{e!>,<aeaia'!<>}}}}}},{{{}},{{<!>},<!!!>!!!>,o,o!!o!!!><>}},{{<}'!!!>!>!!a}>,{}},{}}}}},{{{{{{<>},{<eui'a!!<!>,<ie!!<}!u!>},<!>},<{!>,<!!>}},{{}},{<!!!>\">,<aou!!,!>,<}\"{ee>}},{{}}},{{{{<!!!!\"uou>}},<<'\"!o,e{a!>},<!>,<\">},{{<<!>},<!>},<!>},<i!uu<u!!!!ooie!>},<!>{o>},<a>},{{{},<>},{{<o!!!>ao!!{}{u!>},<,i\"!!>}}}},{{<\">},{{<!!!><!>},<'!!!>\"!>},<\"!>,<,\"\"!}ua!>\"a>}}},{{{},<<!!!>!>,<a!!!>!>},<'oe!!!>,<!>,<!!!!!>!!!!!!i'!uieo>},{{<!!!>!>},<!!o'i}o!!!!ae!!{\"!>>,<!>,<!>},<<e,,!!>},{{<!!!!!>u!!!>{!!oi!>>},{}},{{<a\"!eii!!!aa,,',!>,<io>},{<!!o!>},<,>}}}}},{{},{{{<i!!!!!>,<!>},<'\"}!!!>}u\"o!>,<\"!}<<!!>},{{<io!>,<!!!!a!!!>e!!!>!,!!!>ae!a!!\"<!!}!>,<e>}}},{<<\"!>},<!!>,{<u}>}},{{},{<!oo'!>i!>},<!>,,!!'<!!!>e>}}},{{{<!!!>'{>},<!>,<!>},<}!o>},{<}e!!!>!>,'!>,<!!>,{}},{{},<>}},{}},{{{{<'!!!!!>o!!!,!>,<\"'>}}},{{{<!>},<>}},{},{{<e!u\"'>,{}}}}}},{{{{{{<!<'u}u<!!\">},{{<\"\"ie!\"{>}}}},{<}!!!!e<'ioa!>},<\"!>},<!eo!>},<!>,<!!u>}},{},{{<<<>,{<!{!>},<!!{!!,!>!a!>\"{!!!>!{!!!!iei<>,{{<!!a!u'!!i!aeu!!'!>},<'u!!u\">},{{<!!!>},<,\"oe!<{!,u!!!>!\"!!!>>}}}}},{{<iiia>},<<!!!>!!!!,>},{{<!><!!u>},{<!!!>,<'!>\"!>,<>}}}},{{{{},{{{<aeu!>,<{e!!!>!!!!!\">},<<!!!>},<\"!>u!'{',>},<!!!!!>a!\"o\",!a!'!u}!>},<\"a!>},<>}},{<>,<u!>},<{!!!>,o!!aa\"!!'!!!!\"!!!>ie>}},{{{<!!a!>,<!!u>}},{{{{<!{'!!!!!><{!>ii>}}},<o!!!>!!!>e!!{!>,u!!o!!!!o>},{{<>}}},{{{{<{!!!!i\"i<o!!\"!!ui}}>,<!<!>\"ui\"!!<!>o!>},<!!}!>'\",>},{<!!e!>>}},{{{{{<!!ouee}o!\"!!e!<a!>},<e,}!!'>},<{o<,o\"<a!!!>i!!!>},<,!!!'!!o!>,<u!>>},<<\"!>,<{o{!}i!!<>}}},{{{{<!!{!!!>'a'\"!ei>},{<!!!!!>,<,!!!>'\"e!{<!>,<>}}}}}},{{<\"!!!>ai'>},{{{<!>,<\"ou\"u!>}!!'ue!>},<'o}!>},<a!a>}},<i!>},<\"i,u!!!>!!!!!>i\"{!!!>>}}},{{{{<!a!!!>!o}!!!>},eo!!!>!>},<!>!>},<!>\",!!!!u,>,<!>!!ie,o<',<}!>\"oe!>,<!!>},{{<!!!>!i{u>},<u!!i!>},<'o!}eu{}!e>},{{<\"<u!>!>,<<!!o'!!!!!!!!!!e!,!!!!'!!!>,'!!!>>},<e'!!o>}},{},{{{{{<\"!!!>!!!>}o!>},<u}u}!!e!!!>!!e!!e!!!>>}}}},<!>,<o!!eo!!\"!!!>>}},{{{{<{'!iu!>,<}!>},<}>},{<'i>}},{<}ea!!{,o!!!>,<'}oi<,!}a\">,<!!!>,<!!!!u,\"<i>}},{{<!!!>aei{!>,<!>,<'e<>},{}},{{{},<!>,<>}}},{{<>,<o!>,<i!>},<>}}},{{{{<,\"oo!>},<!!!>i<'!!{!>a!!i\"!\"!!>,<!!i!>i!!a!!!!!>{>}},{{<a>}}},{{{<e{!!!!'>,{<!!o\"{,!!!!>}},{{<iia{e<<a!!!!!!'>},<!!!>},<!!o!!!>,<!>},<\"!!!!!!!>!!!>!!!!uau<>},{<i\"a!!!>a!>{oo,!>},<a!!!>!!ii<}>,{<\"!ia!><o!!!ao!!oe,,i'>}}},{{{<\"o,!>,!a!!e>},{},{{{<>},<!>},<auii!!!><{>},{<!ia<iu'!u!>!!!>},<!>},<o\"!!,>}}},{<!!!>},<!!!!!>'!>},<<!!,\"!!{}<>,<o!!!>!>!>}!!!>!>!!!>!>,<!>,<i!!uu<ou\">},{{<!>u!>!{,a!}!>,<},i!>,<!!!!\"\"a!!!>,<>}}},{},{{},{<}!>e!!>,<!o!>!!!>},<!\"!!u!!!>!!'u!>,<!>>}}},{{{{{<!!{\"<u}!!!!!>!!o!i\"!!!>!>i!!<}o!>},<!!'>}},<<{<>},{{{<!>},<o!>},<i!!!>!>,<'!>},<>}},{},{{<!>{}!>,<!!,,!!,!!\">},{}}},{{{{{<\"!!,}{a!!>},<,!{eau>},{<!o!oo!i}!>,<!o>}},{{{},{}}}},{<ao>},{<,!!!>a!>,<'>,{}}}},{{<!a'!!!!!,!{!>!>,<}!>},<!!\"!!!>}<,>},{{<>},{}},{<\"\"a!!!>>}}},{},{{{{<!>a!>},<<!!ii!!!>!!!,!!>},{<!!oe!{!>},<a'!>,<!!a!,!>>}},{{<!!>},{{<<ouuu,o\",a<!!!}!>},<{{!>o!>,<>},<!!!>,<}o>}},{{<!>},<u!!!!o>,{}}}},{{{{{<a,>},{<!a!!<ii\"<!\"!!!>!!!>!>,<!!!>,aaa!!!>!>,<!!<>}}},{{<!!!>!!'e{!,\",!!!>ii!>\">},{{},<}u{!!!>!!!>!!\"!!!>}!!\">}}},{{<!!u<<e!>iu!!!>!>},<!>,<!>,<>},{},{{<i'!>\"!!!!!>u!!!!!>!>,i>},<i{u!<,}!!}a!>,!>!>!!{\"!!a>}},{{},{}},{{{<!!o!!!!!<u!>,<}!e>},{<a,!>},<!!!!!>!>,<!{!!!>!>!ao\"!a>}},{{{{{<>}},{{},<>},{}},{},{<}e>,{}}}},{}}},{{{<}\"!!!>e!!!><>}},{{<'!!!!\"u'}aou,a>,{<<e!!,'e\"\"!>!>,<{!>},<!!o!>oa>,{{},<u,!!'!!!><\">}}},{{<'!!!>e{!>},<!eo!i!><\",>},{{<!!!>a,u!>!>,<}u!!}!>},<>},<\",!>},<\"!a!!\"!>!>},<>}},{<o!!\"a!>!!}eiii{>,<!!!>!>!>}!>!!!>,<!>!!!!!!i>}},{{{<e}!>,<{!!a!>!>,<!!aoo!>,<ai!>},<>},{<\"o}'\"!uo<!{'!!!>{'o'\"{>,{}}},{<!!\"'\"!!}!>}>}}},{{{{{<!>},<e!>},<>},{}},{<{'>},{<o!!e!>,<>,{{}}}},{{},{<u,o!!!!i!!!>},<\"!>,<{>},{}},{{{<!>\">,<'!!,{!>!!!>,>}},{{{<{<!>},<!\"!>},<'>,<!>o!!!!!!\"<oe{\"<!>!>,<!!aa!>},<>},{<e}\"<!>},<!>ie!e!!\"{auu<oie>}}}},{{{<}>},<uo!!<{\"!!}e'o!>,<a!>},<!>,<!!!>!!!>,<>}}},{{{<<!!!!!!!>!>!>}}{{!!o\"e>}},{{}},{<!>},<!!!a!!'!!!>!>e>}},{{},{},{{<>}}},{{{<!!!>u!!!>eo!o}u!}!{!>},<}'i!>},<>,<,!!!>},<'!!!!!!!>'uu!!a<!>,<!!!>,<!!!>e>},{<u!!!>\"<!!i{aio>}},{{{<!,>},<i>},{<!!!>e!!!>!!!!u}o!!<!!!>\">}},{{<>},{<e!>,<!>},<'!>},<<!!!!!>{}!<}\"!>,<!>},<!>!>e>},{<{'{!>!!ae!!>}}}}}}},{{{{{{<\">},<}oea!>,<}o>},{<!>,<<!oa!!!!}<u,!>!e!!!o>,{}},{{<!!!>!>,<'a{'>}}}},{{{{},{{<>}}},{<!>,<!!!>}eae'!uuo!!}!!<>,{{<\"'i>}}},{<o!>},<}ae!!!>e'{!!!><>}},{{{{<}!>},<!!u!!!>e<'a!!!>,<}ee}!>,!>},<\"<\">,{<e!,!>\"!>!!i!!>}},{{{{<>}},{}},{{<uoi!!<!!!>,<{u}>}},{}},{<>,{<!!!!!!{!!!>,<!>o!!!>},<!!\"!!{o>}}},{{{<!>u!!!>!eoeo{!!<!{!!!>,<}i!>},<>,<,!}a!>,<oa<'>},{<o{>},{<\"e>,{}}},{<!!u!!!{{>,<e!>,<i!!u!!!!!>{!!!>i!!>}},{{{},<!,!!!>,<u!!!>,<'!!!>},<!>},<!>},<!>},<\"!>>}},{{<o!!u!!}!!,!u!!\"a>},{<<oua<au'!!!!!!,!>i!!,!>},<',>}}}},{{{<'ei!!!>{!>,<i!!\"\"<\"!!!>!!!>>},<oauae!!!>},<!!!>ieu!>,<!!u<!!o{!!e!>>},{{{<!ea!!!!!>!<>}}},{<a!!!!a>,{}}},{{{{{<u{>,{{{{<!a!a!>},<!!{!!!>!!\"!>,<i<o\"!{!!!>!!!>},<a,!!!!,>}}},{}}},{{<!!}!!!!!>!>\"{a!!{!>,<!!\"u!!{o>},{<>}},{}}},{}}}},{{{{{<!>,<!>},<u!>o!>e!!,>}}},{},{<!>,<'!!aoa!!!>!!!!!!!>!!!>,<',!!a!>i>}},{{<!\",\"!>!!!>},<oeia,!>}{}\">,{<!!,,e!!!>,<!>,i\"!<oe!><'{!o>}}}},{{{{{<\"e!!i!>>,{<!>!>},<iu!>},<<e'!>!!e<o!>,<!>,<<>}}},{<,\"o!e<<!!!!!eu!!!>o!io!>,<!!'oe!!'>,{}}},{{},{}},{{},{{{<!>},<!!oa!!i!>,<!!ea!!>},<o!>},<!!!>!!<>},{{}},{{{<!>'!!e!>},<!>,<a}!>},<!!!>!!!>\"}}!!i,'>},<{>},{<!>},<<!!!>!>},<!!}>}}},{{{<'i{i'!>!!e!!!>!{ie!!!!!>,<{!>{!!!!a\">},{<!}e!{!>},<ui}{o\"!>},<e!!}!\"\"e''!>>}},{<<o!!!>{i,!{>,{{{<!>!!,>}},{<!!!>!>!!!!}iu,!>},<{o!>},<ai!o}<\"!!!>>}}}}}}},{{{{<!>,<!!{!!!>},<!!<>},{{{{{<!!!>o{>,{}}},{<!!}'e!!!!!ea!>,<{>}}},{{<}!>,<!>,<e!,'\">,{{},<>}},{}},{{{<!>!<{,!>iu{'\"o>,{<!!>}},{{<!!!>u>},{}},{}},{{<i\"!!!>,ee!!''i!><!!'>},<{!>,<!>,,o,o,i!>},<!!!>,<!>}!>},<\"!!!>},<{!!!o>},{{{<!e!u!!!'}!!!!!>!!>,{<o!!!>>,{{{},{{<{!!!>'!>,<!>!>!!o!!!>}!!!>>}}},<!>},<e!>},<!>},<!!!>!>u\"e\"}!a!'a!u{>}}},{{{{<e!!,<!!!!!>,<'{a!>,<}\"!>,<i\"<!>},<a<>,{}},<}u!>u,a!>!!!>u!!!!!>u!<!!',>},{{<,u<'!>!>},<>}},{{{{},<}!>},<{ee\"}\"!>!!<>}},{{<!!a,ae!!!>!!e<'>},<!!!>io!'!>\"!!i!!}!>,<!!o,i>},{<!!!,u'\"!>,<!!!>!>,<!!!>ei!>!!u!>o,!>},<>}}}}},{{<a!!!>{!>,<o!!ea!!},e}!!{>},{{{<e<}>}}}}}}},{{<!!au!uu!!'<\"!}!>u<!!,!!a!>\"<o'>},<}!>},<>}},{{<!!{ao!!!>},<!>,<!!!!'{a!!!>aa!'!!!>>},{}}},{{{<'}<!!!>!!>,<!>!>},<!>,<}!!!>!!a'!!!>}!>},<!!!>!',\"!>}!>},<!>},<>},{{{<!>,<ae{,!a!e!!a!!!>\"{}'!!!>!!!>!!!>{>},{<o,!!\"}!!!>!>'!>ea!!!>u!!i>,{{<\"a<,}!!\",'a!!u}a{}<u!>},<'!!>},<!!}i'!><'!,u<!a}!!i!>>}}},{{<ooo!>},<!>!!!!u'au!!u>},{<>,<!!}}!!!!!>!>u{aeai!>,<!!!!!>!!\"}!!'e!!'}>}}},{{{<!!u>},{<!>,<}o}>,{<ei!>,<!>},<ue'a}!!!>!!'!>},<!>},<i>}}}}}},{{{{{{<!>},<ie!>ao!!\"}o!>},<>}},{}},{<!>,<{!!!!!>}\"!>},<}>}},{{<e\"o!>,<,!>,<!>!!!>,<<o!>,<!!',e>}}}},{{{{{},{}},{{{<!!!>!!!>,<iuu!o'i{,\"o!'ao!!u>}},{{<}<e<!>},<!>,<a<i!>,<!!!>,<a{a>},{<!!!>},<e<o<i!!!>ia>}}},{<e!!}!>,<'<{!>uu!!i<e{i!!!>a>,<!!e'!!!>,<!!!>,<,,<>}},{{{{},{<\"!>},<!!!>},<\"!>,<!>}ai!!!!a!'u>},{{},{<!!!>o\"!>,<!!}o,!ao}!!!>!>!!!!\"{!>o}>}}},{<{!!\">}},{{{<,oooe>,{<!!!>eo<{!>,<!>},<!u<!!!>'!!>}},{},{}},{{<{,<}!>,<u!<!o>},{{}}}},{{<!>,<!>},<!!!>,<}!!'}<o>},{{<!!!'!!!>!uu''!!{,oeu!>,<!>{>,<,\"'>},{{<e!{,o!>},<!!{!oe!>,<!>}\"a\"\"!!!>>},{{},{}}}}}},{{<e!\"{{!},,o\">}}},{{{{},{<'!!\"<!!!>,<<!>},<!>,<!!'\"\">}},<!!!>},<!>}!,,!!ia{>},{{{{{<e>,{<<oe}ii{!e!!!>},<!!<!!!>!!!>>}},{{}}}},<>},{{<e!!!>!!'>,<}!!<<!!!>,<!!!>!!!>,<},i!>!<>},{}},{{<!!!!!>!>,<!!!>'!>,<o!>},<i,!!!>u,}!>}>},{<!>>}}},{{<u,},o!>!!{!>{>},<!o,i!!!e!i!e!>,<aia\",!\"o'!e>}},{{{<!!!!a,e!>\"a!>},<<iu!!!>a!!!>'!>},<}!ii>},{<!,>,{<!>},<!!}<!>!!>}},{{{{<o!>},<>}},<}e!>},<}!!a>}}}}},{{{{{},{<>}},{<>}},{{<!!!!!>\"!!!!!>!i!>!>,<ii!>!>,<!>,<}!!!>,<>},<!>},<!!}a!!!>!!i'!>,<>},{{<u!!o!>},<!>},<<,!!!!a!!<i'u\"a>,<a<{,!>},<'!u!>,<>},{<<!!a!!!>!>!>},<!>e\"i!>>,{<o>}},{<!>,<>}}},{{}},{{<a!!!!o!>!!!u\"!>,<i{i}>,{}},{}}}}},{{{{},{<{<!!!>!>,<u',}!>},<>}},{{},<>}},{},{{<i!!!!<oo,o>},{{{{{<!oa}<o>}},{{{{}}}}},{{<e!!!!!!!!!>,<>}}},{{<{ie!!'!o'!!!>,<!>,<,!>!!{!u!>},<>,<!!!>!!!>!>'<!!!>\"eu>},{{{<,!eu!io<!!\"ui>,{{},<o{eouo<\"!>},<!>,<}<!!eu!!,'!!{{{>}}},{{{},{<!>,<!ea<<,!>!>e>}},{<i!!ii{e!>},<!>},<!!ui!>},<'!>,<\"e!!!>\"\">,{<!!!>>}}}},{{}}},{{<>},<<}>}},{{{},<!!!>>},{{},<!!,a}!>}!!oe'>}}}}},{{{{{<!>,<!!!!!>!!\">}}},{{{},{}},{{<!'!!a!>},<e!>!}ie!!{!!>}}},{{{<!>o<<!!!!<!!{u!>'!e!<!!!>e!>,<>}},{{{},<!!e<!o!!!>!u!!{u<!><!!>},<e!!i!oi'!>,<a}!>,<u!>,<>},{<aeo,!!,!!!>{!>a<!<{'}{!!!>!>!!,>,{}}}},{{{{{{{<i}!!'!>!>,<<!>,<>},<!>},<uaea!!uo!!!>,<'{!!}!!!>,uo>}},{<!>u!!!!!>>,<e{'i!!<!>},<e!>},<a!!!>!!!>,<!!!!!!!>,<!'ei,>}}},{{{{<!a'!!!!!>,!!!>\"\"<!e<!!!>>}},{<!!!><i,\"<!o'<!>,<e!!!!!>o>,{{{{{<'!>},<!!!e!!!>!!<!>o\"oe!>},<,e!!'a>,<!>,<,a!!!u}!!!!!!\"!!i!!'i!!\"u!>},<>},{<<eu>}}},<!!{!>,<!!!>,<i!!!!ee!!!>!!!>>}}}},{{{{{{<}e!>!>o!>!>'i!!\"'<,!>},<{e!!i>},<oiu>},{<,!>},<!u<o!>},<}!>,<!!o>,{<<u\"u!{,!>!!'!>,<<<>}}},{<e!>!!!!o!!}i,<!!!>!!!!!!!>!}'!>!!}i>,{<<!>,<a'!>},<e!!!>,u!>},<e!!{!>>}},{{<!>!!!>e,!a,a!!!>o}!>},<<!>},<a'!!{!!a>},<!!!!<!!!>{<!>,<u'\"!!!>},<<a>}},{{<!>,<{\"!aeoa!!>,<!a!>,<>},{<>}}},{{{<!!!>,<!>,<}e\"!>,<>},{<{!>,<!>,<e!!e!>,<u\",}!!!>>,{}}}},{{<<>}}}}}},{{{{<ie!>,<>},<}u!>},<'ei>},{{},{{<!!i!'o{!{}!>,<!!!>}}!>,<!!!><\">,{<oeo<!!!!!>}{{!!!>{>,<\">}},{{{<\"a!!a!!!}{<>},{{},<}u>}},{{<!>!>'}!!!>!!!>,<!!!><!!{'!!!>!!!>e>},{{{<!>!>,<!>iu>}}}}},{{<a>,{<!!\"!>,<\"'!>,<!!!>},<u!>},<!!<!'i!\"{!!!!!>a!!>}},<<!>,<!>,<}!!!>!>,<!!o!!{!!'i!!!!!>!!!!!>}!u>}},{{<!!'!o{<<!!!>!>,<\"!}}}<}ou>},{<!!!!!>},<>}}},{{<,!!o>}}},{{{{<<!!\"u!>!!!>},<!>,<!!!>o!>},<}!'!!!>{>},{<>}}}},{{{<!>},<>},{<!!!>},<uae'!!!>}i>}},{{<!u''o<>}},{{{}},{{<!!!!eiua>},<u!!i!>!>},<i!>!!{!!!><'!<!>{!>!!'>}}},{{}}},{{{},{<{,'!>,!!!>{!!!>,<!!!!i{e!!!!!>!!!>'!!!>>,{<<!i!>,<{{!i!!oe'a!>i\"!!a<!}}e\">}},{<!!!>i!>},<}!!!>,<'!!!>,<a!>,<,!!!!au!>,<o\"\"e>,{<<!>,<!>,<{'>}}},{{<!!ou!!'!>},!!!>ao!!!>>,{<e<<a!{,a{!!!>!>,<!!}!>},<!!!>!>!>!>>}},{<!!{!!!a<!eu!!'!>!!!>!>}>,<,,,{>},{<!!!!!>,!!>}}},{{{{{<euu!!i\"{>},{{},<o{!>!}\"ea!!!>>}},{{},{<!>,<!!oe}!>},<'{}!!!i!!!>\"!>},<!>},<ie!>,<!!>}},{<o>}},{{{<!>},<e!!oe!>,<e!!!>'a!>u!!<,!!!>>},{<!>},<i>}},{{<!>},<}i!!!!!>e!!!!!>o>},{<>}},{}}},{{{{{<!>i}e'!!!>},<o{!\"!!!!<!!!<{!o>},<{!>,<<a,{i!,!!!>!>,<>}},{{{<!'!!!>!<{,\"o!>,<!!!>!!o>},{<!>i{'!!!!!i'!>,<!!!>}a!!!>},<>}}}}}}}}";

            // example
            //input = "<{o\"i!a,<{i<a>";

            var level = 1;
            int groups = 0;
            int score = 0;
            bool isInGarbage = false;
            bool isCancelled = false;
            int garbageCount = 0;

            for (int i = 0; i < input.Length; i++)
            {
                var c = input[i];
                if (!isInGarbage)
                {
                    if (c == '{')
                    {
                        score += level;
                        groups++;
                        level++;
                    }
                    else if (c == '}')
                    {
                        level--;
                    }
                    else if (c == '<')
                    {
                        isInGarbage = true;
                    }
                    else if (c == ',')
                    {
                        continue;
                    }
                    else
                    {
                        Debug.Assert(false);
                    }
                }
                else // in garbage
                {
                    if (isCancelled)
                    {
                        isCancelled = false;
                        continue;
                    }
                    else
                    {
                        if (c == '!')
                        {
                            isCancelled = true;
                        }
                        else if (c == '>')
                        {
                            isInGarbage = false;
                        }
                        else
                        {
                            garbageCount++;
                        }
                    }
                }
            }

        }

        private static void Day8()
        {
            var input = @"kd dec -37 if gm <= 9
x dec -715 if kjn == 0
ey inc 249 if x < 722
n dec 970 if t > 3
f dec -385 if msg > -3
kd dec -456 if ic <= -8
zv dec -745 if gub <= 4
ic inc 705 if yp > -6
lyr dec -970 if gm != 0
lyr inc 935 if j >= 0
gm dec 716 if gm < 9
kjn inc -897 if j <= -9
j dec -824 if f != 384
x dec 741 if e <= -6
f dec 617 if msg != 9
kjn inc 184 if ic > 697
lyr dec 860 if x <= 707
ey dec -785 if msg < 2
lyr inc -226 if x != 720
t inc -689 if f != -242
riz inc -174 if f != -232
j inc 906 if lzd <= 5
yp dec 264 if zv >= 748
ic inc 578 if t != -694
ucy dec -532 if i < 10
gm inc 294 if i < 6
omz dec 384 if n < 10
ic inc 277 if e > -10
e inc -707 if j != 1740
msg inc 1 if djq > -2
gm dec 625 if ey != 1042
bxy inc 484 if e >= -709
j dec 130 if kd > 29
djq dec 276 if i == 0
ic dec -361 if bxy != 490
ic inc 516 if ey <= 1041
gub dec -382 if e == -707
kd dec 410 if i > -8
tj inc 307 if tj != 5
msg inc -26 if kjn > 186
n inc 227 if kjn <= 191
y dec -920 if lzd >= -4
ic inc -43 if i > -5
ey dec 645 if ic <= 2402
lyr dec -821 if x < 722
x dec 666 if y > 925
n inc 899 if yp < 10
f inc -627 if lzd != 6
m dec 430 if bxy <= 489
gub inc -603 if yp >= 9
ey dec 601 if ic > 2388
e inc 346 if x <= 716
t inc 292 if lzd == 0
msg inc -311 if yp <= 8
lyr dec -599 if zv < 742
n dec 720 if lzd >= -9
t dec 815 if n <= 412
omz inc 175 if msg > -315
ic dec -259 if msg == -310
x dec 250 if kd == -373
n inc 890 if tj < 315
j inc 926 if m >= -425
gub dec -497 if x == 465
f dec -965 if kjn <= 193
ey dec 126 if lyr != 1530
riz dec 95 if ey < -204
tj inc 650 if ey > -222
riz dec -780 if gm != -1041
djq inc -358 if j != 1600
gm inc -506 if gub > 888
i inc -195 if ucy != 539
ucy dec 323 if kjn <= 191
e dec 892 if kd < -368
lzd dec 465 if kjn <= 191
t inc 836 if omz > -216
e dec -994 if gm < -1041
omz dec 974 if ey != -205
kjn inc -844 if f != 99
lyr dec -572 if lyr == 1530
ucy dec 794 if kjn != -669
m dec -139 if y >= 916
f inc -949 if djq == -276
djq inc -148 if m < -289
zv dec 46 if gub == 879
zv dec 303 if bxy < 491
yp inc 145 if msg != -300
gub dec -907 if y >= 924
i inc -88 if gub < 888
tj inc -801 if kd <= -369
n dec -479 if tj == 156
i dec 308 if ucy > -586
tj inc 232 if gm != -1050
bxy dec -845 if f == -843
yp inc -33 if y != 919
ic dec -895 if i == -587
yp dec 321 if tj <= 391
bxy dec -825 if n < 1779
riz inc 497 if t == -376
ey dec -971 if f > -848
m dec -728 if f == -843
riz inc 447 if tj < 396
tj dec 689 if kjn == -662
zv dec 735 if kjn < -656
e inc -317 if djq <= -433
m dec -313 if gm <= -1053
m inc 892 if tj < 394
f inc 367 if ucy != -587
djq dec -374 if zv > -344
j inc 894 if lyr <= 2093
riz inc -802 if gub < 886
gub inc -507 if t > -379
ic dec 894 if riz < 824
gub inc 471 if djq != -57
lzd inc 370 if j >= 1595
gub dec 287 if tj > 386
kd dec 860 if n != 1771
riz inc -133 if t >= -376
msg inc 548 if msg != -310
x inc -941 if ey == 759
gm inc 442 if n == 1766
ey inc 504 if ey == 759
kjn dec 821 if f == -476
n inc -445 if ucy != -586
ic dec 779 if ucy != -577
kd inc 350 if m > 1320
tj inc -525 if msg < -304
x dec -621 if lyr <= 2106
omz dec 39 if kjn > -1485
riz inc -565 if bxy >= 2161
m dec -925 if omz <= -1219
kjn inc 531 if omz != -1224
ic inc 979 if kjn > -953
lyr dec 167 if ey > 1262
x dec 193 if lzd > -90
t dec 261 if lzd <= -102
yp inc 232 if kjn != -950
djq inc 710 if lzd < -89
msg inc 565 if bxy > 2147
yp dec -53 if j <= 1607
kjn inc 868 if x >= 143
x inc 677 if tj <= -140
bxy dec 374 if djq != 651
t inc -609 if e < -256
lyr inc -903 if lzd != -105
lyr dec -87 if omz != -1223
lzd dec -253 if y == 920
yp dec 839 if kd < -884
ic inc -339 if kjn > -83
j dec 588 if bxy <= 1786
n dec -415 if zv >= -336
yp inc -439 if x == 145
riz dec -159 if ic != 2514
lzd inc 61 if f != -476
gm inc -498 if ey < 1270
msg dec 942 if m == 2254
i inc 161 if ucy >= -590
kjn dec 446 if j >= 1006
ucy dec 609 if y > 917
omz inc 558 if ic > 2518
msg dec 675 if ucy < -1185
j dec -962 if t >= -992
n inc -248 if gub < 558
ey dec 92 if y == 920
omz dec -774 if kjn == -528
kd dec -874 if ic <= 2509
kjn inc 445 if j == 1974
j inc -459 if kd >= -884
i dec 605 if lyr < 1129
e inc 995 if zv == -339
t inc 888 if x != 143
kd inc -742 if y == 920
kjn dec 692 if tj == -137
ucy inc 650 if riz > 702
zv dec -539 if gub == 556
m inc 991 if f < -484
lyr dec -260 if riz >= 699
ey dec -916 if j == 1515
tj dec 930 if lyr < 1128
riz inc -261 if f > -470
lyr dec -392 if f != -472
m inc -845 if yp > -602
riz inc -217 if i < -1027
ey inc 35 if riz > 474
gub dec -681 if ucy >= -1198
riz inc -871 if kd < -1634
m dec 793 if y <= 912
ic dec 990 if i != -1045
x inc -178 if kjn > -766
kd dec 952 if gub < 1247
lyr dec 426 if m >= 1407
kd inc -721 if lyr != 1083
lzd dec -887 if omz <= -446
djq dec 648 if bxy >= 1775
m inc 892 if omz <= -444
y inc -757 if yp == -595
lyr inc -943 if ucy >= -1194
ic dec 466 if y < 157
lzd inc 720 if msg < -1354
f dec -746 if kjn != -774
omz inc 234 if yp >= -597
t inc 272 if ucy != -1193
yp dec -237 if y <= 168
kjn dec -360 if lyr != 141
msg inc 246 if n < 1092
djq dec 615 if msg >= -1122
e inc 44 if yp > -359
yp inc -315 if kd <= -3289
kjn inc -987 if ey != 2127
kd inc 784 if m > 2304
yp dec -262 if gub < 1228
riz inc 679 if riz != 485
gm dec -619 if msg < -1107
gm inc 968 if x < 141
zv dec 228 if djq <= -598
gub inc 773 if i == -1035
ey inc -573 if i != -1034
djq inc 791 if gm > -935
t inc -241 if f < 277
t inc 682 if omz != -211
y dec -562 if lzd >= 1760
kd dec 774 if j != 1524
djq inc 843 if lyr == 142
y dec 339 if msg == -1116
kd inc 637 if f > 263
f inc 467 if j < 1519
i inc 662 if riz > 1147
m dec -354 if f == 737
f inc -411 if tj == -1067
j inc -455 if j <= 1524
t dec 7 if j <= 1063
e dec 97 if djq <= 1040
gm dec 695 if f <= 335
gm dec 312 if t >= 614
kjn dec -877 if n > 1089
j inc 770 if t < 618
djq dec 953 if lyr <= 144
ucy inc 595 if gm <= -1620
yp inc -953 if lzd < 1770
i inc -864 if tj <= -1072
lzd dec -68 if yp <= -1622
msg dec 713 if ic == 1524
i dec -632 if yp < -1628
yp inc -779 if omz != -211
yp dec -423 if zv == -28
zv inc 41 if kjn >= -1406
e inc -366 if t != 603
f inc 288 if omz > -215
kjn dec -1 if lyr < 141
yp dec 308 if lyr == 142
lyr inc -999 if zv == 13
bxy inc 904 if kjn > -1410
lzd dec 32 if zv > 8
kd inc 930 if lyr == -857
bxy dec -635 if m < 2661
gub dec 866 if f >= 607
gub inc 163 if gm > -1628
x inc 200 if msg > -1836
e dec 296 if x == 347
e dec 667 if ey == 1549
riz inc 665 if gub == 1307
j inc 37 if zv == 13
m inc 822 if ey == 1549
ic dec 585 if lyr > -849
gub dec -194 if i >= -378
riz dec 123 if y <= 389
yp inc -59 if djq < 84
e inc -199 if djq > 68
lyr inc -685 if yp != -2352
kd inc 573 if j != 1867
n inc 659 if djq <= 86
yp inc 693 if lzd == 1792
ic inc 188 if msg < -1822
t dec 58 if zv != 19
lzd dec 402 if lyr <= -1542
ucy dec -645 if m == 3477
yp inc -47 if msg == -1829
gm dec 910 if omz <= -206
ucy inc 703 if gub >= 1497
lyr inc 907 if e <= -554
y dec 167 if ey < 1555
gm inc -663 if bxy == 3319
j dec -981 if lyr == -1542
lyr inc 638 if i != -380
ucy inc 390 if i <= -373
n dec 68 if y != 223
lyr dec -408 if ic <= 1713
tj dec -856 if n == 1673
yp inc -767 if ey > 1541
omz inc -974 if riz != 1688
j dec 411 if j < 2846
omz inc -972 if djq < 85
e inc -153 if y >= 216
n inc -53 if y < 224
j dec 287 if zv < 6
ey inc -144 if kjn <= -1410
n inc -84 if kd < -2496
bxy dec -788 if kjn != -1408
ucy dec 765 if e < -693
kd inc 846 if ic <= 1716
kjn inc -432 if gm <= -3202
i dec -290 if e > -704
ey inc 625 if x == 344
x dec -338 if msg < -1830
gm dec -621 if y >= 217
j dec 765 if f <= 616
kd inc -559 if riz < 1707
tj dec 498 if j != 2083
bxy dec -414 if gm != -2574
yp dec 365 if msg != -1823
ic inc -143 if m == 3477
ic inc -239 if i >= -88
e dec -479 if yp <= -3524
msg dec 15 if ic != 1337
gm inc -831 if gm == -2573
yp dec -198 if zv >= 12
lzd inc 982 if x != 349
m dec -80 if msg != -1844
e dec 82 if kjn > -1408
msg inc 43 if x > 337
yp dec -204 if msg == -1805
e inc 351 if yp < -3322
omz inc -785 if lyr < -493
x inc 511 if djq != 72
djq dec 686 if lzd == 2381
zv inc -188 if riz > 1692
tj inc 958 if kd == -2211
m inc 951 if gub != 1501
ucy dec 777 if e == 46
tj dec 25 if lyr < -495
ic inc -3 if lyr != -502
yp inc 839 if gm > -3412
tj dec 464 if ic != 1327
gm dec -100 if zv == -175
t dec 786 if riz != 1701
ic inc -727 if gm == -3304
omz dec 755 if yp <= -2487
zv dec -441 if kjn == -1402
yp inc -655 if tj > -240
j dec 272 if ucy <= -406
ic dec -652 if j >= 2088
lzd inc -276 if e > 41
m dec -93 if t > -237
m inc -927 if zv > 264
riz dec -977 if y > 218
n dec 608 if n != 1534
kjn inc 655 if e > 55
tj inc -570 if tj > -239
i inc -677 if t <= -242
tj inc -570 if riz != 2665
msg inc 629 if riz != 2667
bxy inc 247 if lzd < 2111
ucy dec 292 if t <= -226
djq inc -541 if x < 857
kjn dec 211 if lyr == -496
e dec -59 if kjn != -1610
t dec -879 if gm >= -3305
m inc 977 if zv == 266
t dec -103 if x >= 852
riz inc -570 if djq >= -1156
t inc -791 if gub <= 1496
t dec -68 if lzd != 2099
e inc 282 if djq > -1150
e inc -49 if e >= 383
gub inc 137 if gub <= 1503
x inc 627 if j > 2084
djq dec -434 if bxy == 4770
lyr inc -772 if lzd <= 2109
lzd inc 473 if lzd < 2107
gm inc -632 if ey != 1546
x dec -342 if omz != -3702
kd dec 108 if m > 3619
e dec -180 if i <= -76
kd dec -69 if e >= 511
e inc 286 if n >= 926
e dec -507 if t <= 815
j dec -718 if ic != 600
lzd dec -439 if tj > -1382
ic dec -876 if tj <= -1370
y inc 511 if f > 618
f inc -100 if x != 1208
t inc 671 if ucy <= -689
gm inc 47 if e > 1314
f inc 609 if gub <= 1633
gm inc 409 if y > 213
zv inc -420 if e > 1302
ic dec -304 if omz < -3696
x inc 698 if kd != -2250
n inc 805 if zv > -158
tj dec 84 if j != 2084
ucy inc 41 if ic <= 1779
j inc 597 if ey == 1549
gub inc -483 if msg != -1172
ey dec -535 if omz > -3710
riz dec -877 if gm >= -3529
riz dec 117 if f < 518
m dec 275 if i <= -79
kjn inc 742 if j <= 2680
kd inc -472 if omz != -3703
e inc -865 if lzd <= 3018
x inc 187 if omz != -3710
bxy dec -675 if tj >= -1466
zv dec -635 if tj >= -1451
riz dec 822 if f >= 508
m inc -460 if i == -76
tj dec 623 if j >= 2677
m dec 515 if djq > -1155
ic dec 392 if ucy != -695
tj inc 29 if omz < -3691
yp inc -94 if zv != -158
bxy inc -237 if ic >= 1778
n inc -204 if lzd > 3016
bxy dec 320 if x < 2086
f dec -545 if y > 216
f inc 771 if djq != -1153
i dec 94 if msg > -1175
djq inc 681 if m != 2835
kjn dec 176 if kd < -2727
i dec 559 if msg > -1172
kjn inc -475 if omz > -3708
riz inc 912 if kd < -2725
x inc -636 if tj == -2054
e inc -508 if gm >= -3528
f dec -7 if ucy != -704
ey inc -764 if y > 213
lzd inc 282 if ucy == -695
ic inc -146 if gub >= 1645
kjn dec 149 if x < 1442
bxy dec 638 if lzd >= 3296
omz inc -699 if riz >= 2952
ic dec -638 if kd == -2727
ucy dec 107 if e <= -55
kjn inc -449 if zv == -154
y dec -239 if x <= 1455
gub inc -778 if n <= 1530
omz inc -239 if riz > 2952
y dec 465 if x < 1448
n inc -916 if msg <= -1165
yp dec -10 if lzd >= 3297
kjn dec -881 if yp <= -3224
j inc -634 if ic <= 1782
msg dec 178 if ic >= 1780
ic dec 678 if ucy != -795
kjn inc -90 if n >= 610
lyr inc -396 if n <= 616
riz dec -95 if tj >= -2056
zv dec 570 if e < -55
lzd inc -437 if tj >= -2062
zv dec -453 if t <= 1487
msg dec -597 if yp == -3230
m inc -467 if kd < -2723
omz inc 662 if f < 1839
kd inc -956 if lzd > 2861
f dec 155 if x == 1443
e dec -89 if ey < 1323
t dec 694 if ucy >= -806
gm dec -345 if j <= 2055
y dec -331 if gub == 860
m inc -947 if gm < -3175
zv dec 381 if f >= 1832
msg inc 545 if yp != -3239
ucy inc 17 if gub < 862
tj inc 906 if ic < 1103
y inc 674 if lyr == -1664
omz inc 409 if x < 1451
omz dec -108 if djq == -466
kd dec -554 if gm <= -3180
f dec -178 if gm > -3186
ucy dec 186 if msg >= -208
m dec -339 if j != 2040
e dec 813 if y <= 991
lzd dec 484 if zv < -649
gub dec -639 if omz > -3576
n dec 527 if ey >= 1313
msg dec 519 if t == 792
djq inc -976 if msg >= -729
riz dec 31 if riz == 3050
kjn dec -234 if gm != -3177
bxy dec 946 if m >= 1755
yp dec -652 if x < 1455
y inc -685 if t > 788
x inc 801 if n <= 86
djq inc 251 if y > 304
kd inc 213 if lzd == 2378
gm inc 642 if tj >= -1150
n dec -560 if yp > -2574
kjn dec 380 if ucy < -965
e dec 651 if kd > -2926
ic dec -775 if y >= 306
msg dec 527 if ic <= 1881
zv inc -872 if m != 1749
tj dec -337 if f != 2022
djq dec -440 if gub <= 1489
e inc 231 if lzd != 2386
kjn inc -259 if tj <= -819
e inc -588 if djq < -1185
e inc -970 if kjn > -1327
gm inc 904 if n != 83
y dec -32 if j <= 2055
riz inc 888 if y >= 343
ucy inc -874 if kd != -2918
tj inc -154 if omz == -3567
e dec 44 if m >= 1751
msg inc -659 if n >= 87
f inc 854 if t <= 794
lzd dec 938 if f != 2872
kd inc 298 if lyr != -1658
yp inc 196 if bxy < 3311
lyr dec 328 if zv != -1520
zv dec -919 if riz >= 3904
ey dec 236 if riz >= 3900
ic dec 998 if tj >= -966
ucy inc -518 if riz < 3910
djq dec 332 if riz > 3904
m dec -853 if ic > 870
t dec -173 if tj != -962
gub inc 332 if t >= 962
t dec 366 if ey == 1084
j dec -298 if bxy < 3311
n dec 495 if y >= 338
lyr inc 283 if djq == -1525
yp dec 705 if riz != 3898
m dec 632 if zv < -598
yp inc -219 if i < -184
gub inc -865 if yp < -3080
bxy inc -940 if t >= 596
yp dec 217 if ey >= 1078
y dec -242 if ic > 870
riz inc 438 if bxy >= 2358
x dec -923 if y == 587
lzd dec -832 if kjn > -1327
y inc 25 if kjn <= -1319
kd dec 573 if m != 1976
gub dec 134 if kd == -2624
n inc 291 if zv <= -599
n dec 226 if msg == -1256
msg inc -646 if omz != -3565
i inc 454 if kjn != -1333
tj dec -123 if ey <= 1090
gub inc -428 if tj == -842
gub dec 456 if i > 283
t inc 270 if ic >= 875
x dec 671 if n != -111
lzd dec -137 if e != -2003
y dec -868 if lzd < 2414
gm inc 935 if e > -1999
j inc 981 if gub <= 538
m dec -967 if lzd != 2410
gm dec -448 if kd > -2629
f dec -625 if lzd > 2403
t dec 760 if omz >= -3559
tj inc 688 if n != -118
gm dec 351 if e <= -1994
t dec 915 if f <= 3501
riz dec 336 if gub < 541
e dec 775 if e > -2001
j dec -467 if bxy <= 2364
bxy inc -66 if riz == 4009
gm inc 979 if e == -2770
i dec 579 if ey > 1086
m inc 349 if x > 2503
zv inc -79 if lyr == -1709
riz inc -493 if ic < 888
lzd inc 308 if i != 273
gub dec 527 if yp >= -3297
t dec -667 if kd == -2620
gm dec 785 if yp > -3308
riz inc -465 if kd == -2620
kd dec 608 if n > -125
e inc -901 if gm > -411
ucy inc 23 if x > 2493
ucy dec 246 if tj <= -834
tj dec 735 if lzd == 2716
t dec 635 if gub >= 538
lyr inc 107 if gub != 538
lyr inc -624 if djq <= -1521
lzd dec -958 if msg < -1892
ic inc 648 if msg < -1892
gm dec 329 if i <= 280
y dec -335 if ey <= 1086
lyr dec 19 if x != 2504
gub dec 220 if f >= 3489
kjn dec 449 if msg == -1900
ey inc 292 if f != 3503
ey dec -671 if n <= -120
kd inc 516 if kjn <= -1780
i inc 208 if riz < 3048
x dec 737 if f >= 3500
ucy dec 767 if lyr != -2352
ey dec -958 if t == -7
yp inc 340 if gm == -739
ey inc 545 if kjn == -1775
x inc -678 if omz == -3567
n dec 862 if zv > -685
ey dec 674 if yp > -2970
y dec 31 if t > -19
ic dec 196 if lzd < 3685
t dec -470 if lyr >= -2355
msg dec -814 if f > 3492
omz inc 638 if f == 3494
ic dec -297 if bxy == 2299
y dec -185 if ey >= 1244
yp dec 462 if lyr >= -2357
j dec 777 if ucy > -1717
i dec 834 if i <= 274
kd dec 851 if m <= 2952
ey dec -738 if bxy != 2289
djq dec -702 if ic > 1328
f inc 674 if msg >= -1094
msg inc 290 if kd != -4080
gub dec -235 if yp < -3417
omz dec -750 if m > 2937
kd dec -870 if x != 1823
i dec -983 if omz <= -2171
kd inc -637 if m != 2942
ic inc 253 if x >= 1816
kd inc -790 if x == 1814
lyr dec 369 if yp > -3436
j inc 72 if riz < 3058
j inc 55 if y == 1967
msg dec 788 if ucy <= -1711
riz inc 138 if gm < -730
kd dec -953 if n == -981
i inc -225 if omz == -2179
bxy dec -21 if msg != -1574
m dec 737 if f <= 4170
ucy inc -304 if lzd != 3675
gm inc -199 if ey != 1979
tj inc -468 if m > 2204
ucy dec 143 if djq > -833
djq inc -587 if m > 2205
f inc -213 if gm <= -929
msg dec -56 if gub >= 555
gub dec -662 if msg == -1584
omz inc 94 if t != 456
kd dec 995 if msg >= -1587
ic inc 803 if t < 454
omz inc 897 if x == 1822
j inc 534 if zv > -685
ic inc -997 if riz < 3186
yp inc -903 if gm > -945
lzd inc 326 if ucy != -1862
m dec 374 if x > 1829
tj inc -655 if gm < -932
n dec 89 if x > 1812
djq dec -619 if lyr >= -2729
j dec -969 if n == -1069
lyr dec -218 if i >= 1033
gm inc -897 if i > 1031
bxy inc -615 if yp >= -4335
omz inc -147 if tj == -1964
y inc -81 if msg != -1591
j dec 569 if yp <= -4325
kd inc 715 if ic != 1590
djq inc 661 if ey > 1976
tj dec 272 if tj > -1967
x inc -72 if zv == -684
ucy dec 338 if ucy >= -1846
msg inc -542 if lyr != -2495
zv dec -535 if ey >= 1978
m inc 944 if lzd != 3992
e dec -944 if gub >= 1213
gm inc -525 if bxy >= 1701
e inc 894 if tj != -2239
f inc -237 if lzd > 4007
x dec -248 if bxy > 1706
gm inc -484 if lzd > 4008
i inc 332 if f != 3947
yp inc 853 if kd > -4125
ucy inc -743 if gm <= -2351
djq inc -901 if gm <= -2358
t inc 76 if m < 3148
msg dec 945 if gub >= 1213
tj dec 411 if zv != -149
m dec 904 if m > 3141
msg dec -416 if gub <= 1220
x inc -354 if ey >= 1994
i dec 971 if gub >= 1206
ey inc -446 if gub >= 1215
lyr dec -106 if bxy > 1697
t inc -177 if ey <= 1547
lzd inc -147 if lzd >= 4003
ey dec -765 if f == 3953
y inc -678 if gm < -2353
x inc -7 if ic <= 1591
lyr dec 449 if lyr > -2407
gub inc -58 if ucy > -2605
n dec -512 if zv >= -156
lzd inc -847 if n > -560
tj dec -136 if djq < -1022
t inc 824 if bxy <= 1709
djq dec -358 if msg != -2663
zv dec -212 if t != 1106
zv dec 6 if ey > 1538
msg dec 302 if n < -554
ic dec -784 if i == 402
m dec 450 if tj == -2101
kd inc -559 if j != 4018
n inc 611 if riz == 3179
n dec -995 if kjn > -1781
ic inc -411 if ey == 1539
kjn inc 140 if t == 1113
y inc 247 if t < 1104
omz inc -241 if djq >= -675
omz inc 463 if kjn <= -1774
kd dec -93 if bxy > 1696
ucy inc 256 if e <= -1828
j inc -134 if t > 1096
gub dec 900 if tj != -2101
ic inc 254 if yp >= -4334
n dec 416 if n <= 446
kjn dec -636 if ic >= 1434
riz dec -348 if lzd <= 3147
gm inc -604 if n <= 26
gub dec -253 if zv != 62
m dec -274 if n >= 26
tj dec 487 if gm != -2960
omz inc -1 if n >= 25
gm dec -830 if kjn > -1782
kjn inc 617 if ey != 1539
e inc 152 if msg != -2957
x inc 627 if t >= 1109
yp inc 191 if ic >= 1436
kjn dec 985 if y > 1448
f dec -899 if f > 3951
yp inc 92 if gm <= -2127
e inc -615 if yp > -4240
x inc -443 if x != 1745
lzd inc -472 if y <= 1463
t inc 421 if djq >= -677
n dec 833 if ey == 1539
e dec -168 if omz < -1050
djq dec -670 if t >= 1523
omz dec -361 if gm > -2138
i inc 918 if ucy <= -2333
e dec 946 if ey > 1534
y inc 150 if tj == -2588
y dec 740 if yp <= -4231
gub inc -4 if gub <= 1412
riz dec 778 if f < 4858
msg inc -733 if e == -3226
i dec 377 if omz >= -708
msg dec -710 if x != 1301
j dec 380 if zv >= 57
ey inc -702 if ey > 1529
e dec 830 if omz < -699
gub inc -104 if riz == 2411
j dec -501 if gm < -2130
bxy dec -176 if kjn < -2758
zv inc 603 if zv >= 57
lyr inc 921 if lyr != -2839
j inc -337 if kjn < -2755
e dec 245 if ey > 836
t inc -339 if djq < 0
ucy dec 65 if yp <= -4237
msg dec 87 if lzd < 2687
ucy inc 949 if yp < -4227
lyr inc -298 if riz < 2416
ucy dec -52 if n == -811
tj inc -776 if bxy <= 1886
msg inc 650 if djq < -6
djq inc 542 if omz > -697
y dec 131 if zv != 660
tj inc -184 if gub <= 1303
djq dec -223 if ucy != -1401
gm inc -746 if gub < 1305
m dec 726 if zv != 662
djq dec 134 if kjn != -2754
zv inc 693 if tj >= -3538
msg inc 327 if omz == -699
gub inc 913 if x != 1294
yp dec -183 if gub == 2215
bxy inc 558 if tj < -3540
msg dec 278 if m >= 1069
j dec -760 if gub < 2212
gub inc 720 if n != -820
bxy dec 458 if msg == -3018
tj inc 630 if ucy <= -1399
i inc -489 if bxy == 1978
x dec 651 if gub > 2934
f dec -23 if lyr < -2216
y inc 744 if n == -811
ucy inc -624 if j == 3661
riz inc -947 if m == 1070
y dec -103 if ic < 1430
m dec -158 if f < 4884
y inc 897 if j > 3679
kjn inc -307 if bxy == 1978
y inc 130 if kd <= -4589
msg inc -597 if bxy > 1972
tj inc -575 if t > 1185
i inc -325 if msg <= -3624
i inc 834 if gm >= -2889
ucy inc -431 if yp != -4054
t inc 499 if j >= 3669
msg dec 39 if ey == 837
lzd dec 774 if kd != -4592
x inc -468 if j < 3681
x inc 223 if gm <= -2872
t dec 74 if djq >= 95
djq inc 306 if m > 1221
i dec -669 if kjn <= -3065
omz inc 92 if bxy == 1978
gub inc 762 if tj >= -2911
gm dec -810 if kd >= -4590
ey inc 797 if i >= 1943
t inc -20 if tj != -2913
yp inc -66 if x < 395
omz inc -270 if omz > -617
x dec -365 if msg >= -3656
msg inc -443 if t >= 1662
msg inc -334 if lzd > 2674
f inc -478 if riz == 1464
m dec 551 if lyr <= -2222
yp dec 321 if m >= 684
yp inc 930 if ucy < -1400
j dec -836 if ucy >= -1404
lzd inc 941 if tj <= -2916
tj inc 890 if j > 3668
kjn inc -538 if ic > 1423
i inc 79 if ucy > -1401
kjn dec -145 if e <= -3464
ey inc 851 if i > 1948
msg inc 566 if ic >= 1423
riz dec 615 if bxy != 1988
x inc 929 if msg == -3868
ucy inc -486 if gub != 2933
ic dec -842 if t < 1663
lzd inc 453 if f < 4405
riz dec 591 if ic != 1437
lzd dec 457 if e >= -3474
gub dec 221 if n <= -802
gub inc -707 if riz > 256
bxy dec -352 if riz == 266
x inc -754 if j >= 3667
tj inc 624 if y == 1852
ey dec 96 if kjn <= -3453
j inc -682 if t > 1673
j inc -830 if zv != 652
zv dec 263 if ic > 1420
m dec -636 if ic > 1435
lzd inc 806 if yp < -3114
yp dec 886 if zv <= 402
t dec 758 if ic < 1419
riz inc -212 if omz != -876
djq dec 815 if kd == -4589
t dec -70 if gm >= -2889
ey dec 798 if gm <= -2877
i dec 879 if zv < 407
f dec -946 if i < 1082
i dec -419 if y == 1844
lyr dec -544 if kjn > -3461
gm dec -138 if kd < -4582
gub dec 371 if lzd < 4416
n dec 455 if kd <= -4583
e inc 475 if kjn <= -3457
lzd dec 393 if riz >= 45
x dec 8 if kjn > -3456
gm inc 546 if yp >= -4015
riz dec -65 if msg < -3858
lyr inc 684 if omz >= -867
tj dec 642 if j != 2833
riz inc -100 if lzd < 4025
y dec -178 if lzd <= 4024
f dec 614 if y > 1840
kd dec -708 if kd == -4592
m dec -324 if x <= 15
gub dec -402 if kd > -3893
omz inc 27 if zv > 395
bxy inc 323 if msg == -3865
ucy inc -74 if i <= 1499
kjn inc 969 if yp >= -4016
lyr inc -253 if n == -1266
msg inc -109 if riz == 111
msg dec -216 if m > 995
kd dec 767 if t >= 1726
lyr inc 689 if omz <= -843
yp inc -108 if yp >= -4001
i dec -696 if ucy == -1966
e inc -230 if gm >= -2197
omz inc -695 if msg != -3749
y dec -658 if ic >= 1430
y dec -933 if ucy == -1972
m dec -207 if tj < -2664
gub inc -537 if yp >= -4018
msg inc 324 if kjn > -2489
ey dec 165 if n > -1275
djq inc 738 if zv <= 403
m inc -432 if j <= 2848
ic inc -881 if omz < -1537
y inc 159 if gub == 1879
kjn inc 236 if djq == 1130
i dec -648 if djq > 1122
lzd dec -793 if j == 2838
bxy dec -33 if j != 2845
ucy dec 290 if kjn >= -2257
msg inc -81 if djq == 1130
t dec -466 if djq < 1129
djq dec 402 if tj == -2670
gm inc -693 if lzd != 4022
j dec -327 if zv < 403
f dec 978 if djq <= 728
djq inc -35 if y >= 1837
msg inc -609 if t == 1734
tj inc 64 if n == -1266
f dec 465 if t != 1727
f dec -48 if omz >= -1553
kd dec -533 if y < 1850
t inc 959 if y != 1850
yp inc 764 if i <= 2837
lyr dec 822 if bxy == 2334
msg inc 723 if m > 768
djq dec -846 if ic >= 555
n dec 17 if ic < 551
m dec -661 if kjn != -2254
tj dec -975 if f != 3334
bxy inc -920 if i != 2844
e inc -222 if x == 15
lzd dec -413 if yp > -3241
i inc 953 if y <= 1849
riz inc -675 if kd != -4112
zv dec 433 if e >= -3450
bxy inc -302 if gub >= 1870
x inc -645 if zv < -37
omz inc -471 if kd >= -4115
bxy dec 450 if riz >= -573
omz dec 274 if e != -3452
f dec -92 if i >= 3786
ey inc -10 if f < 3419
djq inc 120 if lzd > 4027
lzd inc 360 if yp >= -3246
n inc 175 if djq < 820
tj dec -641 if gm <= -2889
tj inc -3 if lzd != 4393
lzd inc 116 if msg == -3725
djq inc -421 if kd < -4110
e inc -509 if gm != -2880
omz inc -878 if bxy >= 655
kjn dec -877 if djq < 400
ucy inc -697 if yp < -3237
ucy inc -656 if gub <= 1874
y dec 642 if msg <= -3735
kjn dec 679 if djq <= 385
ey inc -768 if gm != -2898
ic dec -433 if kjn == -1387
x inc -150 if t < 2699
ic inc 89 if j < 3175
m dec -623 if riz != -565
msg dec -165 if omz != -2693
omz inc -668 if djq != 390
tj dec -812 if riz <= -557
zv dec 563 if f <= 3430
riz dec -319 if lzd == 4518
y dec -58 if i == 3789
ic dec 166 if riz != -562
ic inc -587 if zv != -603
riz dec -462 if ey != 664
gub inc 153 if ucy <= -3604
t dec -106 if ey <= 658
msg inc -821 if zv < -593
i inc -340 if e <= -3955
tj dec -771 if djq < 388
ic dec -437 if n < -1106
e inc -273 if lyr == -2065
bxy dec -912 if m > 2055
n dec -215 if gm == -2889
yp inc 716 if e == -4230
j inc 548 if gub > 2024
f dec -77 if x <= -126
omz dec -487 if kd < -4127
msg inc -384 if ic <= 321
j dec 97 if t > 2797
i dec -197 if x < -135
lzd dec 491 if lzd >= 4503
ucy inc -338 if f <= 3508
tj dec -932 if msg == -4765
y dec 273 if omz == -3365
y dec 50 if djq > 386
lzd inc 958 if m <= 2069
ey dec 449 if m <= 2067
lzd dec 143 if y != 1511
gm inc -413 if lzd <= 4837
n inc 129 if ucy == -3947
tj inc 527 if bxy >= 1571
msg inc 448 if i == 3440
x dec 73 if yp == -2530
t inc 608 if djq != 393
gm dec 937 if omz > -3368
kjn inc -435 if msg >= -4766
m inc 511 if m < 2067
t inc -410 if f >= 3498
x inc -137 if ucy == -3947
riz dec -965 if ic == 319
kjn inc -528 if riz <= 860
ic dec 582 if tj != 1288
msg inc 859 if m > 2564
f inc 573 if bxy != 1573
kd inc 970 if yp > -2534
f inc 390 if gm > -4248
ucy dec 0 if bxy < 1582
kd inc 814 if zv <= -591";

            // example
            //            input = @"b inc 5 if a > 1
            //a inc 1 if b < 5
            //c dec -10 if a >= 1
            //c inc -20 if c == 10";

            var rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            var parsedRows = rows.Select(x =>
            {
                var m = Regex.Match(x, @"(?<register>[a-z]+) (?<instruction>[a-z]+) (?<value>-?[0-9]+) if (?<condRegister>[a-z]+) (?<condOperand>[^ ]+) (?<condValue>-?[0-9]+)");
                Debug.Assert(m.Success);
                var register = m.Groups["register"].Value;
                var instruction = m.Groups["instruction"].Value;
                var value = int.Parse(m.Groups["value"].Value);
                var condRegister = m.Groups["condRegister"].Value;
                var condOperand = m.Groups["condOperand"].Value;
                var condValue = int.Parse(m.Groups["condValue"].Value);
                return (register, instruction, value, condRegister, condOperand, condValue);
            }).ToList();

            var registers = new Dictionary<string, int>();

            int GetRegisterValue(string name)
            {
                if (!registers.ContainsKey(name))
                {
                    registers[name] = 0;
                }
                return registers[name];
            }

            int ComputeInstruction(string instruction, int registerValue, int amount)
            {
                switch (instruction)
                {
                    case "inc": return registerValue + amount;
                    case "dec": return registerValue - amount;
                    default:
                        Debug.Assert(false);
                        throw new Exception();
                }
            }

            bool ComputeCondition(string operand, int registerValue, int amount)
            {
                switch (operand)
                {
                    case ">": return registerValue > amount;
                    case ">=": return registerValue >= amount;
                    case "<": return registerValue < amount;
                    case "<=": return registerValue <= amount;
                    case "==": return registerValue == amount;
                    case "!=": return registerValue != amount;
                    default:
                        Debug.Assert(false);
                        throw new Exception();
                }
            }

            int largestEver = int.MinValue;

            foreach (var instruction in parsedRows)
            {
                var registerValue = GetRegisterValue(instruction.register);
                var condRegisterValue = GetRegisterValue(instruction.condRegister);

                if (ComputeCondition(instruction.condOperand, condRegisterValue, instruction.condValue))
                {
                    var newValue = ComputeInstruction(instruction.instruction, registerValue, instruction.value);
                    largestEver = Math.Max(largestEver, newValue);
                    registers[instruction.register] = newValue;
                }
            }

            var largestValue = GetMax(registers.Values.ToList()).MaxValue;
        }

        private static void Day7()
        {
            var input = @"bqyqwn (68) -> wscqe, cwxspl, syogw, xnxudsh
ddswb (34)
hnkvw (320)
ibqmynm (252) -> oglcdgs, tkjofj, upurae, oypvhy, bzfkt, hdvcz, cfwxyl
rkerea (87)
lcmlbj (66)
vtccvv (69)
nachvlp (20)
tmkli (66)
exuyuk (82)
ojzogs (79)
egkiqcp (37) -> rxjnad, psetts
xadjes (10)
fivkqxx (121) -> acvzsbe, kkmax, qcmwsvm
krgtrdn (53)
mxemqqb (267) -> iuuouds, qqmvd
iwwqvoa (24)
yyecrv (59)
zjpkoar (40)
ilsgqdw (37)
bmlfd (43)
nxqwag (81)
ubbwvvl (44)
khfzis (48) -> ztxcc, bvejguc
gvtcy (39)
xbxxe (173) -> bzjxil, amklj
qejgaf (25)
uunpyb (82)
dmidur (57)
fklfgd (65)
vcjfe (22)
rkjfx (33)
ioglbe (88)
tymkixg (36)
qbhhtw (90)
afbrhy (79)
nswcez (47)
wzjonl (16)
xviqup (95)
claolm (312) -> ndmkbul, rpjfkh
qhlhfk (387) -> iwwqvoa, rmshk, ftezz
vtokttx (55)
boalcdm (13)
angpy (62) -> fitha, itxgz, pyiexlj, wqshh, rloxobx
gwcdf (32)
jpscxfh (46) -> qsakd, exuyuk, dqdmie, kjwfeoj
qkzpcyy (96)
fifams (415)
yseezd (11)
qcmwsvm (37)
hfnhd (152) -> vdcfh, ufrhi
wfgzr (82)
sjoov (6)
fonrb (7531) -> xohxq, cmsqe, jefsr
pyhof (21)
pplis (9)
fosdh (90)
pjkyydd (15)
wvmtyez (46)
bzfkt (66) -> rauho, xctuhbx
zyqssjb (13) -> olebdv, byahs, xajzwdh
wotus (119) -> dndrnc, tebkmhi, qejgaf
rusndcc (39) -> xcfxvd, kvylxr, vwkegak, oiuztx
stofi (16)
mgqil (66) -> noibcw, nptqxtu
qjzss (31)
hawhqex (71)
kjjee (174) -> lltgjo, gwcdf
gdamb (99)
diqdy (183) -> gackyrq, vcjfe, lcnsf
sfwzqk (54) -> rkuzg, ewvvb
vktjns (45)
urwaex (59)
nyouzhg (17) -> lofybs, baedi
mklqpn (9)
iktget (33)
gxtfu (57)
xkymbld (37) -> mqgbif, ptxfk
qjeuqe (92)
benqzp (92) -> wvmtyez, gmveehb
rkctaq (20)
sbwoye (50)
bujufb (148) -> tduxvx, lxzvkpk, jllqymn, csrpy
cwfydbh (369) -> qcmtgvr, tyvjs
cjjytkr (305) -> trszl, fgzgfc
qsakd (82)
domway (2464) -> kgphv, pjujovx, hrdggo
mrpji (61)
kzrgfb (92)
hocazx (818) -> bmukspb, mgbwxw, gyirkcj, kgxfc
bobvssz (25) -> srvqt, alhghui, wnjaw, qhlhfk, lsbvzaf, qjkfys, nnncfl
rrzyzq (76)
eaufvn (78) -> dtazpb, nlajc
tduix (18)
yzcez (62) -> rlxto, vkaxa
uvpuko (91)
xtnmps (95) -> ptatzl, bujufb, oybbzr, zwopz, phqpwkw
ibeis (54)
ofkvpq (500) -> auvuxv, stqmwk, dptekr
pmolg (166) -> uuunqk, uolqt
byedxex (180) -> gsaqlad, ebwubc
aicjff (67)
vtcxw (860) -> ezlfnhs, aizdt, ylbftp
nzuswm (80) -> ojzogs, qfopwo
hgwbg (89)
sflonq (190) -> gppwsbp, mpvgy
pqiuhyp (52) -> xbtjl, jykseco, aftnyad, xaden, urdgg, ijkpfv, ejekn
ccgjf (99)
devsz (38)
vmmtsr (33)
ebwubc (70)
mgcnohi (69)
klege (92)
jtlqxy (66)
ofbqk (148)
ewluica (22)
mulvwn (290) -> ubbwvvl, jmuqp, dnovxt
trlcj (245)
uezwz (20)
bbtnj (170) -> xvwok, rqyxzn
ndticx (54) -> rbtco, bobvssz, umtxk, domway, wugia, ubvttg, ydpemw
lxwkpqh (325) -> xoigv, fqsjaz
mpvgy (35)
ujgeml (94)
jkoir (6)
kemkz (98)
dtqgdc (90)
girxc (147) -> zylotoh, qtxmjwm
olawkuc (79) -> earmfdp, wnowypb
umrqldb (99)
oyurlfq (57)
flgpg (188) -> jimltsw, xhgza
wawtrk (80)
nlajc (85)
ewqcktv (1364) -> hfnhd, amqvr, ntxcpwl
dmvyuf (35)
vtdbov (50)
xnszvs (65)
mjrktxe (50) -> eoxwp, jgsmitm, isrgei, rnbgz, kymbvpp, ktjdpt, mgzocpe
zicvok (81)
tkmfbk (93) -> ffhsg, kynezb
vlwvot (84)
oybbzr (1007) -> qstlv, nayvvrh, izxpf, habni, ckfefl, fjdlcbh, thhakxn
qiksfr (44)
wowxv (92)
fmdba (44)
zgxbjj (96)
jncjxue (66)
xquhgig (190) -> rqhfxty, maftysp
habni (27) -> xwknab, xukfcd, scvhl, tcbgf
iftcnc (50)
dqdmie (82)
otlvq (40) -> hjsrvj, rtbcch, vpirde
mgzocpe (51) -> zkwajbd, mrpji
fkzxb (89)
jlsmm (32)
igdkn (43) -> dgtxjuj, jxyakll, kptdem
rftuf (43)
uolqt (9)
rtwlo (78) -> wgbvle, npttye, claolm, ydffzgu, mgbgb, jwjuth, mulvwn
nrgzcge (132) -> bywbrk, vazoq, eaufvn
juoaena (94)
wfsaglg (54) -> nlfdps, tdkdiqe
jfsgqq (100) -> crslvc, esized
mcucysr (49)
tcbgf (28)
mlalrpt (166) -> jtbrjm, fyaxzl
srvqt (155) -> jyftx, dtijfqc, phcqntb, uyhrcfl
xohxq (1457) -> czbydf, wgdnyy
lqbmor (230) -> rkjfx, vmmtsr
jjsyexg (38)
ynvesz (88) -> rqaxn, afntec
ibbxb (194) -> imiimt, llikz
ktasnia (49) -> thkjz, cjgtra, tzngdx, cqjsqh, vtjtyp, lyzaead, xrtzs
vsjjq (30)
yjnzgl (67)
mcrhiy (6559) -> mkgafux, gugxitg, tymmo
nlgymop (139) -> tymkixg, crchwpb
ybosi (18)
rhqfb (179) -> okurp, gpwbcg, invqsb
kzjozhi (90)
ixncawy (28)
ooczbeh (36)
ubvttg (796) -> wuuvnlz, donzb, bwgimi, rywtmt, rusndcc, ceagctk
ohcoit (6)
exrud (176) -> hpziqqg, abxglwt, gozhrsf, oqjqu, vhkodl
opvrgh (79)
blyms (46)
vfrylx (10) -> qhkniqm, bmcwbsi
ndmkbul (55)
mohey (86) -> tbmyue, ibeis
wktzh (93)
ystgyng (50)
jclntv (87)
dsjcqpn (133) -> slufzy, gbbpqj, jxawk
fitha (68) -> faqngn, xguqqao, athuly, fmdsfus
xyang (225) -> njxrb, mfwuqb
acjedny (49)
mfwuqb (60)
ayuma (81)
qqmchoq (7)
yzjhmqq (171) -> dinpz, upozls
qfopwo (79)
srypr (64)
tvwmzal (236) -> cpujsn, ewluica
umtxk (2942) -> jyazs, scnhwcs, fmpvv, aahqvqy
gkwyfz (49)
dodrbx (227) -> uhtnl, vxnsova
sfdclf (238)
kksizqf (44)
dfofyir (236) -> ddswb, grxdbx
qakiz (37)
ceagctk (255) -> bwvlnlt, rkazlp, vbejzc, njhlrtm
hnjszii (284) -> zkdwz, bwlit
gbbpqj (34)
bvaoy (114) -> mogwbda, rodntl
vkihn (36)
symdxo (8)
nduildw (30)
nxhfgx (18)
chgfb (16)
gugaaiq (40)
euvrdg (79)
ckzaj (67)
jupzgjm (90)
llalaql (27)
qdnghwn (16)
iffluz (515) -> bbbvmxo, reomle, vspqi
nvrjzg (214) -> nzfko, qqmchoq
xvwok (34)
enmjipp (95)
jtbrjm (18)
rlxto (83)
xpotznx (41)
lslhph (59)
vhkodl (221) -> otlvq, fioegdo, rreidgp, fivkqxx, xahvbm
mncivb (88)
hbhfdlh (89)
mkgafux (688) -> admjq, tqtut, emkgc
tymmo (22) -> uxjaa, nlgymop, dcvfkk, yzjhmqq, iqjqlfq, faopk
vxslvq (37) -> jmvcgo, ioglbe
rxjnad (80)
ejdxula (92) -> kviiza, uqmaluk, nasbs
xvzoy (35)
vukpdsr (162) -> gumncu, mlzmimg, vjobpzv
xoigv (10)
iuehdsr (21)
jzgvab (9)
odxpup (89)
vwkegak (92)
bsvgagj (41)
mxwgu (310) -> yeyxf, qdnghwn
jynmg (1140) -> wotus, mohey, fiynyre
qftwuv (36)
veabcj (67)
zaargo (30) -> ytrsmfx, jtlqxy
xfbfvuu (65)
gpwbcg (5)
elysbb (81)
engpslr (85)
tqbtvqc (31)
faqngn (98)
jykseco (166) -> xawzbz, fsdoohr
nfywcyp (62)
rreidgp (168) -> vfupn, xyvnk
obmcr (89)
spnllx (76)
mbbuqoi (35)
wjxgg (177) -> symdxo, dsptf, yaxeloo, yxrhis
chcjswk (89)
anamgt (346) -> ywwxi, rnbvid
zjwqc (26)
wxgoew (1481) -> hnjszii, ygqxl, xhkwrs
dydcy (76) -> drnki, uxolsws, lrcstqr
npttye (42) -> wkrgvo, jhydv, enmjipp, hnyjyga
yjwvnhl (219)
fykgs (43)
vrmqym (97) -> lrmnb, nvigovl
gkfdg (226) -> ohcoit, jkoir
ictsk (122) -> devsz, jjsyexg
gixezcy (37)
rywtmt (158) -> nevrtrs, sybvdj, qcorw
pwhtvdh (40)
yapfoe (59)
xnnbqju (90) -> xzsmw, petvm, xyxidwf
vxiwms (99)
htaszsq (87)
mgbwxw (52) -> afbrhy, jrsfu
rdzsisf (59)
qvxanzu (262) -> nxhfgx, vvzmdr, inddon, ybosi
ysbhsn (181) -> wleavrv, kmvvxon
tirifqs (6489) -> ofbqk, iqdiez, kspsz, kejph
rnkbli (84)
bjbndbg (169) -> oqsdzpe, sbwoye, yhukqf
dndrnc (25)
oxzhz (86) -> bcbftl, hjzgzoe
ufrhi (68)
gsyeaes (26)
kgbzu (180) -> wzczb, bzqjb
dgtxjuj (97)
xulgx (5)
wleavrv (16)
qnhbmzp (118) -> wktzh, zodwdpq
csrpy (62) -> zmoxc, ccgjf, umrqldb, gdamb
ldqyt (50)
btwmd (54)
volompq (96)
qmshniw (443)
lrmnb (98)
yijwaf (32)
paoxff (98)
ulbute (95)
lyzaead (939) -> jfqddfd, ucpkpfb, sbmwazv
rodntl (60)
tykcvi (9)
aftnyad (188) -> rfqddq, recxxj
zdelqb (155) -> jaasf, snqnh
akwcv (65)
nszqngg (44)
eavas (45) -> ytsmc, mrxdp, prjpkm, nzawh
fqvbdq (96) -> tzimies, jduij
mabcqcb (194) -> itutdol, vtokttx
trvtkqj (49)
ynlzggk (36)
pggjo (19)
jajdx (19)
ackyirz (16)
gtiopzn (49)
idvnxqp (5)
njsjq (273) -> vptdap, eteyvz
qkjdjpe (44)
dzfon (61) -> wksjlix, zicvok, utiwem
nxmhned (166) -> pekohn, xnszvs
pnqycz (43) -> gkkit, khyca, yudzxoa
lvkeqm (192) -> lneyu, uopxjn
qvrvcu (19)
hvwyae (23)
ocgbpez (114) -> gjgrhw, bvtgvu
mehuxr (53)
pmdcajx (7)
ybixf (299) -> xvhgvw, ooczbeh, ksbwlir, pkzwosb
inlft (36)
hshafy (65)
nzfko (7)
vcnael (95)
nxuer (13)
pulql (59) -> akwcv, uijffti, ysxvzdu, xfbfvuu
hlbotz (13)
zhrdezs (28)
aqxaecv (297)
ppjapn (27)
bbbvmxo (21) -> rceksnv, uhnlse, rswnjx, ogcmm
ikvzux (44)
hdvcz (156) -> zisdcf, keurs, bjops
ytsmc (22)
jiwfia (13)
jthpj (44)
dtijfqc (76)
vgfhpwb (99)
sjmhuxn (11) -> aceyee, iohigm, zildb, vrmqym, pxxabj, gjjfx
byahs (65)
qllime (202) -> dspkk, wgtnepi
ksbwlir (36)
hzawlq (16)
kgwftu (87)
qtzelhq (62)
wabzx (209) -> sfazwz, lnndami
dyazidi (219)
resoi (25)
hgwkyu (91)
urdgg (166) -> srypr, fnayjx
hsfhqhe (17)
metif (68)
ifxzdfy (96) -> ssnpxj, ejhojmh
jefsr (477) -> fqvbdq, xsygbj, rcluk, dcero, lfgqgs
oglcdgs (140) -> dmvyuf, xvzoy
jrhzmbj (45)
mlvhqpe (91) -> qsstg, tnrrui, utsnke, gxtfu
iffxdk (87)
xboqv (17)
hppxlu (174) -> lkjhmiq, kntwy, auhnyn
pralyx (191) -> dmkfnm, xcmzg
osnkrr (53)
nhvyso (77)
mogwbda (60)
rqaxn (96)
vxnsova (5)
xsxnizc (50)
hskhxx (36)
ogcmm (57)
drnki (25)
ecjxs (197)
dnniv (16)
wnjaw (369) -> brvxyyg, hsvmept, praagwp
xjcxk (24)
aahqvqy (74)
hpwaux (97)
sadyx (59)
dptekr (112) -> xiqthw, ihtfo
mlmhe (4563) -> uwxjkq, uqals, ofkvpq, pyxdkm
yytmc (66)
hmbuu (16)
ezzpr (94)
zpxgfq (118) -> tqplqn, fysxwmd
hgkmod (75)
earmfdp (83)
mxpgmte (5)
kcfgbv (73) -> fdeowqq, pbzvjg, tzrqwt
jtxjj (120) -> hiuviy, sadyx
dsptf (8)
dsezlbe (54)
onzind (245) -> wayzzn, ystgyng
pjndde (422) -> yseezd, cseckdt
rnbgz (131) -> iuehdsr, xgzluhk
yswonja (195) -> gachtig, bjadb, gtiopzn
piizso (59)
msqxo (47)
jacmj (29)
hjsrvj (64)
prcxksh (20)
aceyee (219) -> gixezcy, zqwqcnq
pkipy (95)
czbydf (75)
bpguivk (32)
utapoe (84)
qjdou (87)
njhlrtm (38)
pkzwosb (36)
wpphwjv (83)
teqej (66)
lneyu (64)
dicalia (44)
opzxu (46)
gasejp (15)
llikz (22)
uhtnl (5)
kntwy (25)
wzbewii (157) -> myetb, dhwakp, vqtalpm
vkaxa (83)
kwnqw (49)
uhnlse (57)
zqamk (37518) -> mflijpx, ikzap, wzimze, agjpn
fnnykbh (91) -> nxqwag, tpcbt, elysbb, xlaqpg
ytrsmfx (66)
pyiexlj (92) -> oozbxqb, kzrgfb, klege, csfjt
yaxeloo (8)
tzbmy (71)
pjecv (56)
gsaqlad (70)
gmzcjo (76)
cuscn (114) -> fmdba, ikvzux
rbzmniw (52959) -> tespoy, tirifqs, exrud
jzoxv (52)
egizyey (78) -> metif, towuup
uxolsws (25)
twyhx (86)
nqjydpi (44) -> rpgzp, nszqngg, jthpj, qiksfr
avecfoq (95)
wltax (21)
xfyqx (20)
tnrrui (57)
gmveehb (46)
kymbvpp (67) -> qzvobt, osnkrr
xbccghy (66)
iqdiez (134) -> wkkmvcn, nkzgse
pmopji (128) -> gpcyq, qsdyww
cfwxyl (210)
khzqqwi (12)
wjrhp (992) -> nyouzhg, ccxff, qmifv, oonpy, vutdji, owukmji
yvlajs (35)
hkltivy (26)
vylzag (33)
mieynp (21)
jppbvbo (929) -> ynvesz, tvwmzal, mbwaxn
yxxgd (296)
utiwem (81)
ihvefep (110) -> ffzzzc, ckzaj, zfyqzi
vfupn (32)
jhydv (95)
vspqi (201) -> wzjonl, dnniv, pdazlob
iohigm (248) -> icxzod, mclqrmw, pjkyydd
hzehjo (43)
kwhgtnk (78)
npxexv (66)
lycfbo (89)
vezhmd (47)
amqvr (274) -> aditjen, xvdmeo
vbqpp (35)
upurae (33) -> rdzsisf, lslhph, urwaex
uvkwg (88)
wapknj (2443) -> xulgx, idvnxqp
mvgmf (105) -> dicalia, qhkhuow, phnuqn
qmifv (163) -> hzawlq, ackyirz
xlacmu (87)
aesiz (86)
jjiikvt (25)
prjpkm (22)
rbtco (1110) -> dfofyir, qnhbmzp, dzfon, zmdkea, valby, mabcqcb, mifwbmm
greav (20)
dmkfnm (30)
ftezz (24)
iuuouds (14)
jwjuth (390) -> hmbuu, oxlzlor
gzwrv (212) -> efqjc, gasejp, qqooh
wgdnyy (75)
rqhfxty (53)
qhpvuzi (75)
cvxqnh (53)
stqmwk (96) -> vcnael, dhobx
wuuvnlz (206) -> pznkdcr, ucfipvi, jfhzgp
nnyws (190)
bbbhx (5)
qdfvw (69) -> hgwkyu, uvpuko, wclnx
inddon (18)
gyirkcj (138) -> inlft, vkihn
lrtzi (53)
pjlxc (62)
slufzy (34)
zkdwz (20)
tigvdj (44217) -> xtnmps, ylnobx, mlmhe
vptdap (23)
aemkbp (35) -> dwrtbo, rkerea, iffxdk
dpngy (17)
nzhbfc (207) -> hvgozl, hsfhqhe
wayzzn (50)
myhnuu (66)
jrsfu (79)
gqeiy (257) -> opvrgh, sibvtz
xgzluhk (21)
fiynyre (156) -> mieqogh, zpaivc
vqtalpm (26)
thkjz (437) -> insleik, ayjse, sgovqnf, wjxgg, euwab
ylbftp (82) -> epcjds, gsyeaes
ndikjqv (22)
tqplqn (36)
aifpxqv (89)
hjbfr (20)
lltgjo (32)
picnycy (33)
kplvk (24)
ptatzl (314) -> sfdclf, ibbxb, slzpby, kjjee, bbtnj, gkfdg, nzuswm
qtxmjwm (7)
ckudisa (78)
opskc (35)
qhkniqm (96)
rqyxzn (34)
vdcfh (68)
hpziqqg (637) -> ypqkl, nmazby, hxswr
vrfme (20)
ozdnanh (34)
qcmtgvr (12)
gvdlvgg (20)
krnbkr (82) -> qladcl, tefdmi, ocdpr
fyxttac (39)
hbbpp (95)
hjzgzoe (38)
hjqsj (85)
asevk (44) -> hpwaux, byojhfr
fgzgfc (44)
ejtdhx (57)
ntxcpwl (36) -> kerjk, nggubzz, utapoe
fqsjaz (10)
hxswr (74) -> bixxpzd, htaszsq
dinpz (20)
yxxdal (44)
wozakri (251) -> mgcpbp, omslcm
dhwakp (26)
kvylxr (92)
lsbvzaf (75) -> ibqrz, cokvree, volompq, zgxbjj
bixxpzd (87)
xbtjl (294)
dyfzj (95)
utsnke (57)
sxjlr (91)
ckfefl (115) -> khzqqwi, fzwzr
ezlfnhs (62) -> qftwuv, ynlzggk
xguqqao (98)
jazqb (7378) -> xadcuw, hocazx, cqvdpy
dsmjo (32)
gpcyq (55)
dhvsrt (7)
jllqymn (110) -> dzwfsix, paeel, jclntv, qjdou
zkgdtp (90)
mjqkaqs (50)
nzcqte (140) -> wkkdc, pwhtvdh, gugaaiq
woypnze (91)
dcriqom (79)
zppujp (34)
aqacmr (18)
rkazlp (38)
fysxwmd (36)
mzutqi (571) -> qdfvw, mxwgu, yswonja
zfyqzi (67)
wqshh (460)
rztur (86)
bdlnohy (584) -> dodrbx, qwotm, cbavt, mvgmf, igcmpn
qqmvd (14)
uwxge (94)
ijkpfv (218) -> rxcgwd, steez
slzpby (136) -> nxxlbnt, ozdnanh, zppujp
dzuqljn (7) -> spnllx, fpeiss, hjzkjuw, pjoqde
dporud (50)
hrdggo (152) -> agztuz, mehuxr
kigyp (65) -> hjqsj, ijtis, engpslr
qcorw (83)
cmsqe (12) -> njsjq, zqpxn, pulql, fkttd, wozakri
keurs (18)
pyxdkm (872) -> xnnbqju, oxzhz, zaargo
pjhpwh (35)
vdlbwyo (24)
ocdpr (44)
gacseeh (89)
xzsmw (24)
liujmc (41)
myetb (26)
xawzbz (64)
qmyfg (114) -> weipv, woypnze
heisrl (36)
mlzmimg (24)
ayjse (75) -> veabcj, aicjff
htitjjt (203) -> nswcez, msqxo
xhkwrs (324)
tdkdiqe (87)
ukgbdva (65)
qmcphv (32)
fmhfnhv (24)
nhjtvk (47)
sbmqxu (25)
ewobo (25)
hvgozl (17)
ysxvzdu (65)
sfazwz (68)
hlwesdc (24)
wgbvle (422)
ugzmeb (77)
hbptt (138) -> uezwz, gbckkh, gvdlvgg
auezw (95)
lkuwf (95)
cneno (190) -> uwjobru, jeadxth
wkrgvo (95)
koxzeku (79)
vehziv (67)
codkti (71) -> tqbtvqc, oausk
kjwfeoj (82)
dcvfkk (111) -> uahcz, xsxnizc
lcnsf (22)
zzwmn (40)
bywbrk (80) -> rnkbli, vlwvot
jpmrs (94)
lfgqgs (187) -> vbjpz, hlbotz, boalcdm
xbnmg (48) -> aesiz, tqelui
pcvfgti (29)
ucpkpfb (181)
blkewbk (64) -> jlsryou, idokb, avecfoq, ozyqew
mbwaxn (240) -> nachvlp, prcxksh
wgsgaj (17)
fzwzr (12)
mgcpbp (34)
jxyakll (97)
fdeowqq (77)
zxmwy (35)
yudzxoa (84)
wwbebbr (95)
wgtnepi (66)
wkkdc (40)
xlccnkv (13)
yxrhis (8)
uwjobru (22)
csfzl (64)
dspkk (66)
xwknab (28)
fmdsfus (98)
vpirde (64)
nulax (10)
ymkgx (23)
psetts (80)
ucfipvi (67)
hsdym (340) -> onjqcrh, eitdb, ycuuwxa, mxemqqb, pnqycz, wexncjl
wqojj (22)
xiiqh (98)
cqjsqh (360) -> bzmqg, jpscxfh, ercxmph
wnowypb (83)
snqnh (95)
mifwbmm (114) -> tfezflv, qxrspl
fcgkmq (68) -> thftfut, svtqdbd
gachtig (49)
qxnvoh (214)
ihtfo (87)
qxrspl (95)
nnciip (95)
ercxmph (318) -> ixncawy, zhrdezs
iqmpc (53)
rnbvid (49)
xukfcd (28)
bjadb (49)
niifkl (90)
kxrbd (20)
lrcstqr (25)
gackyrq (22)
cbavt (161) -> qvrvcu, uhuees, pggjo, jajdx
mahlbe (6)
llpbev (29)
efqjc (15)
efvqwzi (7)
epqowm (45)
fjdlcbh (139)
csfjt (92)
zrnlkl (63) -> dyfzj, xviqup, atrzkqf, ulbute
xsygbj (130) -> vdlbwyo, ciquv, msdcz, kplvk
cdfmatu (80)
xeihyok (81) -> uunpyb, wfgzr
ssnpxj (56)
wclnx (91)
nnncfl (335) -> sgytd, nreuwh
cdjxwb (99) -> buznuv, hkltivy
aditjen (7)
auhnyn (25)
nvigovl (98)
yeyxf (16)
uwxjkq (905) -> jblwph, cdjxwb, dydcy
lkjhmiq (25)
teygd (170) -> llpbev, nrxyk
jaasf (95)
dwrtbo (87)
hwgwzv (57) -> qqqbf, fnslg
alhghui (279) -> qbhhtw, ltytvpb
mjqzvuu (195) -> hvwyae, tmzxl
wzczb (61)
ciquv (24)
gozhrsf (762) -> ifxzdfy, uuxpvxf, zyqssjb
khyca (84)
atrzkqf (95)
psjec (97)
xyxidwf (24)
esized (69)
towuup (68)
trszl (44)
fkttd (141) -> hbhfdlh, hgwbg
olvztw (21)
tebkmhi (25)
ltytvpb (90)
msdcz (24)
csdap (27)
vqinar (2904) -> qhbrmur, nrgxdmx, angpy, gcswiam
yxbaldn (122) -> vxiwms, syvza
swgjx (6)
mflijpx (75) -> oxypy, rtwlo, vwnvlh
pygrhr (144) -> rdorrx, pjecv
alneot (25)
wdbmakv (42933) -> ktasnia, mxnaq, mcrhiy
fjticim (35)
rsfxf (123) -> yjnzgl, glrmew
tyvjs (12)
gbckkh (20)
dgqfrv (35)
oozbxqb (92)
oypvhy (140) -> yvlajs, pjhpwh
fnslg (52)
vhzfut (96) -> obzrtar, qtzelhq
iqjqlfq (59) -> gmzcjo, rrzyzq
amklj (23)
tvbgr (80)
qsrxouy (96)
rauho (72)
tzrqwt (77)
pdazlob (16)
uhuees (19)
qebqgxd (94)
ffhsg (79)
hzuyh (50)
oiuztx (92)
wjdeth (5)
bjops (18)
bwvlnlt (38)
baedi (89)
irokors (39) -> xiiqh, paoxff
zvtfm (41)
aizdt (54) -> zjpkoar, zzwmn
ffzzzc (67)
oxoul (12) -> fkzxb, chcjswk
dcero (34) -> ssvfbfs, nxbec
rloxobx (421) -> jiwfia, nxuer, xfcktep
foznw (146) -> dpngy, wgsgaj, xboqv
buznuv (26)
tduxvx (326) -> iktget, vylzag, picnycy, lfdery
hiuviy (59)
dtazpb (85)
oxlzlor (16)
bvejguc (68)
uwjkolt (52) -> tvxvtce, igdkn, rteyxil, qvxanzu, qllime
rvqopbu (67)
owukmji (195)
pbzvjg (77)
agztuz (53)
zpaivc (19)
nofpmh (290) -> efvqwzi, pmdcajx
mhnnmp (8) -> qbaosu, pjndde, anamgt, bcesxba, blkewbk
zfqmic (67) -> lkuwf, pkipy
tespoy (5) -> bdlnohy, jppbvbo, fgsmzi, sjmhuxn
qhbrmur (1450) -> kcfgbv, dyomhm, nofpmh
lkdyq (57)
fuaio (16)
ccxff (53) -> cgveph, tzbmy
fioegdo (140) -> opzxu, blyms
bgnfpcr (75)
bmukspb (106) -> jzoxv, gqvmeg
sbmwazv (39) -> arrvxtj, hawhqex
xqwfgy (1496) -> cywvxf, bjbndbg, mlvhqpe
jfqddfd (64) -> ewgadin, fyxttac, gvtcy
empixc (142) -> hlwesdc, xjcxk, fmhfnhv
zmdkea (154) -> bgnfpcr, hgkmod
jeadxth (22)
ucilvib (40) -> ddgnyfo, koxzeku
rteyxil (334)
clqfbvi (66)
lfdery (33)
ijtis (85)
rmshk (24)
kptdem (97)
ropdc (226) -> usevjww, agthzo
talptwp (77)
bzqjb (61)
idokb (95)
kynezb (79)
tvxvtce (52) -> ujgeml, jpmrs, lpsaav
athuly (98)
vazoq (248)
rxcgwd (38)
tfezflv (95)
cywvxf (279) -> greav, kybtla
lfliwg (65)
qjkfys (353) -> bjhxuqc, cvxqnh
vvzmdr (18)
gwuixj (31)
svtqdbd (61)
nqfqgj (22) -> niifkl, fosdh
lxzvkpk (458)
gewpohj (16410) -> cvqbwem, uzljl, zlhpt, vtcxw, iffluz
bggovyb (144) -> supjxi, mieynp, wltax, pyhof
pbdafpz (236) -> wjdeth, zacwh, mxpgmte, wjgjhc
pqrpkmd (34)
maftysp (53)
qqqbf (52)
fpeiss (76)
qsstg (57)
oqeam (128) -> qjzss, gwuixj
brvxyyg (30)
oqmxlu (39)
tlpfhx (147) -> heisrl, hskhxx
icush (877) -> rsfxf, zfqmic, axzszf, gzwrv, cujuai
jagajrh (93)
kejph (98) -> alneot, jjiikvt
acvzsbe (37)
gqvmeg (52)
olebdv (65)
tqtut (74) -> ykitsi, wxvjyn
tzngdx (486) -> pvyrkg, hppxlu, eovobtl, diqdy
vmsoc (61) -> euvrdg, dcriqom
qiznqk (77)
kspsz (112) -> tduix, aqacmr
kybtla (20)
ktjdpt (23) -> tdmirp, qhpvuzi
jmvcgo (88)
hjzkjuw (76)
qinne (35)
ypqkl (64) -> qjeuqe, wowxv
epcjds (26)
mrxdp (22)
pznkdcr (67)
kmvvxon (16)
xlaqpg (81)
nhtfal (23)
bxqqvue (5668) -> zqbpucs, mhnnmp, ewqcktv
ipnvjje (80)
noibcw (95)
pywtg (16)
omslcm (34)
sxdngcn (6)
insleik (209)
wxvjyn (63)
ntezb (25)
klkwsa (57)
figvamv (32)
phqpwkw (1192) -> egkiqcp, ecjxs, foznw, tokyi
onjqcrh (67) -> lkdyq, tptqtpq, dmidur, klkwsa
igytdcp (160) -> vtdbov, dporud
vrtgae (66)
bwgimi (242) -> eohjrft, ankrgg, kvvkbhi
bzjxil (23)
ankrgg (55)
okurp (5)
rndvcf (39)
rkgoovn (104) -> qyfsc, lytrpo
thftfut (61)
jduij (65)
invra (180) -> sxdngcn, mahlbe, swgjx
wkkmvcn (7)
igcmpn (55) -> bcadp, sxjlr
pgaiut (90) -> vtccvv, mgcnohi
iytvm (15) -> juoaena, qebqgxd, uwxge
glrmew (67)
uqmaluk (70)
qstlv (75) -> qmcphv, bpguivk
gtbgp (234) -> ilsgqdw, qakiz, zesfmu
rziopi (57) -> flgpg, nqjydpi, xbnmg, xxdcgas, vhzfut, sfwzqk, kirnt
rdorrx (56)
oxypy (1992) -> sflonq, igytdcp, nzcqte, sjbfmfa
pekohn (65)
praagwp (30)
qqooh (15)
nzawh (22)
ywwxi (49)
phnuqn (44)
ylnobx (5204) -> rziopi, mzutqi, lcitl
gumncu (24)
cjgtra (900) -> nfkfex, rhqfb, oyyctv
gjgrhw (94)
paeel (87)
xiqthw (87)
ogowst (176) -> pdzooh, xlccnkv
mevujmj (83)
tlqjsb (994) -> odxpup, znagag, obmcr
ejoxsy (38) -> lxwkpqh, zdelqb, xyang, gtbgp, onzind, wabzx, kzjtb
agthzo (15)
xyvnk (32)
crchwpb (36)
pjoqde (76)
fgcajp (50)
tefdmi (44)
xctuhbx (72)
tokyi (79) -> yapfoe, yyecrv
njxrb (60)
qotyaq (7)
dnbejx (70)
uhrdeol (171) -> qinne, opskc, zxmwy, dgqfrv
cwxspl (12) -> nnciip, hbbpp
nggubzz (84)
uuunqk (9)
lxttas (133)
xvdmeo (7)
gkkit (84)
wexncjl (295)
jgsmitm (173)
pxxabj (33) -> wgupfl, lpdhdvy, lfliwg, fklfgd
ruedk (146) -> kgwftu, xlacmu
ydpemw (3192) -> ymkgx, nhtfal
ibqrz (96)
oyyctv (34) -> ipnvjje, tvbgr
xfcktep (13)
jblwph (81) -> lxexhp, vbqpp
steez (38)
xnxudsh (78) -> pjlxc, nfywcyp
rxjly (133) -> rftuf, hzehjo
eteyvz (23)
eohjrft (55)
wqilo (32)
tbmyue (54)
nkwwia (1405) -> dsjcqpn, irokors, wzbewii
oausk (31)
rpjfkh (55)
mxnaq (3937) -> glcsinj, icush, wjrhp
kviiza (70)
anggtqq (86)
uuxpvxf (114) -> nhjtvk, vezhmd
yrolq (64)
tkjofj (60) -> mjqkaqs, lnfjj, nakli
nasbs (70)
usevjww (15)
valby (106) -> vgfhpwb, xtnvc
zxygmaq (10)
tdmirp (75)
zodwdpq (93)
uyhrcfl (76)
mqgbif (73)
oqjqu (67) -> xbxxe, yjwvnhl, vmsoc, tlpfhx, rxjly, dyazidi
euwab (49) -> cdfmatu, wawtrk
admjq (40) -> qvaayfl, uqynzy
petvm (24)
zqpxn (196) -> himyt, zvtfm, xpotznx
nxxlbnt (34)
kpbawoe (26)
zlhpt (74) -> zvbqni, iytvm, htitjjt, aqxaecv
kkmax (37)
recxxj (53)
ejhojmh (56)
dnovxt (44)
crslvc (69)
fnayjx (64)
lytrpo (47)
auvuxv (242) -> wqojj, ndikjqv
zesfmu (37)
cnffplo (166) -> fuaio, pywtg
dfmjy (399) -> isxysg, dxlpr
xcfxvd (92)
xznhz (54)
mclqrmw (15)
cqvdpy (62) -> yzcez, nvrjzg, sebvkw, pgaiut, bggovyb, wfsaglg, teygd
fewgct (90)
xaden (108) -> jagajrh, fucxhz
xvhgvw (36)
ejekn (98) -> kemkz, souqntk
hsvmept (30)
zwopz (60) -> byedxex, yxbaldn, hnkvw, lvkeqm, kigyp, ruedk
uqynzy (80)
axzszf (167) -> iiuqtqw, vktjns
gugxitg (553) -> trlcj, olawkuc, xeihyok
wugia (2689) -> temju, xkymbld, ntlrkw
lxexhp (35)
scnhwcs (74)
yhukqf (50)
upozls (20)
qladcl (44)
wjgjhc (5)
fucxhz (93)
nayvvrh (25) -> ejtdhx, oyurlfq
oonpy (15) -> wyryqvy, zkgdtp
xowkw (41)
zildb (116) -> umjcr, pinqp, piizso
hrrld (1802) -> pqiuhyp, nkwwia, exsmez, rarkt, hsdym
lzojty (214)
uzljl (710) -> pmolg, benqzp, khfzis
vyinb (154) -> fzyim, vrfme, hjbfr
syogw (36) -> mevujmj, wpphwjv
qyfsc (47)
wvvivob (6)
wksjlix (81)
reomle (191) -> jacmj, amcixd
fzyim (20)
zfipnh (21)
uopxjn (64)
gaxdd (25)
dzwfsix (87)
nrxyk (29)
vbjpz (13)
kgxfc (30) -> kzjozhi, dtqgdc
nfkfex (134) -> vsjjq, nduildw
cpujsn (22)
weipv (91)
wjceo (184) -> resoi, gaxdd
unkkoph (119) -> myhnuu, lcmlbj
lnfjj (50)
bmmzqxb (81)
gzocw (110) -> qkjdjpe, yxxdal
uxjaa (131) -> coapfqs, kxrbd, rkctaq, xfyqx
jimltsw (16)
nptqxtu (95)
rpwdig (10)
xflhoc (20092) -> bqyqwn, hycgb, nrgzcge
jfofam (6042) -> gewpohj, ndticx, xflhoc
syvza (99)
whrinsh (34)
coapfqs (20)
souqntk (98)
rpgzp (44)
wgupfl (65)
jxawk (34)
zeolg (214)
qzvobt (53)
nmazby (120) -> yijwaf, idmqvse, dsmjo, wqilo
yuiyjpc (862) -> eavas, lxttas, codkti
rkuzg (83)
gdvuw (179) -> vrtgae, teqej
pinqp (59)
xtnvc (99)
gppwsbp (35)
exsmez (1357) -> pralyx, tkmfbk, unkkoph
ewvvb (83)
nlfdps (87)
jmuqp (44)
mieuhe (97)
donzb (407)
igbzlxl (27)
invqsb (5)
xxdcgas (30) -> auezw, wwbebbr
qhkhuow (44)
sgytd (62)
ikzap (5388) -> mjrktxe, tlqjsb, yuiyjpc
kvvkbhi (55)
xahvbm (222) -> udgssh, bbbhx
iiuqtqw (45)
isrgei (173)
ntlrkw (36) -> gkwyfz, acjedny, mcucysr
qxugs (31) -> iqmpc, lrtzi, krgtrdn
sebvkw (214) -> qotyaq, dhvsrt
gcswiam (1183) -> vahmn, cwfydbh, cjjytkr
ncuzpin (39)
sybvdj (83)
obzrtar (62)
thhakxn (103) -> pplis, tykcvi, mklqpn, jzgvab
vtjtyp (152) -> dhisacj, zpxgfq, oqeam, fcgkmq, nnyws, qxugs, oxoul
icxzod (15)
nakli (50)
bzmqg (374)
dhisacj (73) -> rndvcf, oqmxlu, ncuzpin
pdzooh (13)
pvyrkg (51) -> yytmc, clqfbvi, tmkli
scvhl (28)
qwotm (169) -> pqrpkmd, whrinsh
dcpzwz (87) -> wxgoew, xqwfgy, hrjlzxr, wapknj, ejoxsy
qsdyww (55)
puvmdie (41)
umjcr (59)
himyt (41)
tptqtpq (57)
zkwajbd (61)
vutdji (31) -> bsvgagj, puvmdie, liujmc, xowkw
lpsaav (94)
bmcwbsi (96)
rtbcch (64)
nadbegy (66)
jyazs (74)
xcmzg (30)
abxglwt (191) -> pmopji, jtxjj, asevk, yznmd, jfsgqq
tzimies (65)
jfhzgp (67)
wyryqvy (90)
kirnt (134) -> bmlfd, fykgs
xhgza (16)
xajzwdh (65)
tpcbt (81)
zmoxc (99)
bcbftl (38)
arrvxtj (71)
imiimt (22)
tqelui (86)
sibvtz (79)
pchfpqp (86)
faopk (147) -> figvamv, jlsmm
wscqe (14) -> ezzpr, chyun
emkgc (142) -> lcrbt, pcvfgti
nreuwh (62)
vahmn (49) -> twyhx, rztur, pchfpqp, anggtqq
eourjv (204) -> kpbawoe, zjwqc
ssvfbfs (96)
dxlpr (8)
sgovqnf (69) -> kzsoasg, dnbejx
eoxwp (133) -> zxygmaq, xadjes, nulax, rpwdig
ydffzgu (380) -> olvztw, zfipnh
byojhfr (97)
nlwakg (161)
ztxcc (68)
zisdcf (18)
itutdol (55)
cseckdt (11)
zqwqcnq (37)
fhqhu (188) -> igbzlxl, llalaql, ppjapn, csdap
kzsoasg (70)
cvqbwem (92) -> wjceo, gqqsfsr, vukpdsr, cneno, bvaoy
pjujovx (96) -> ayuma, bmmzqxb
vjobpzv (24)
kerjk (84)
qvaayfl (80)
bcesxba (444)
hnyjyga (95)
gjjfx (131) -> btwmd, xznhz, dsezlbe
idmqvse (32)
ygqxl (16) -> nhvyso, talptwp, qiznqk, ugzmeb
ozyqew (95)
itxgz (370) -> epqowm, jrhzmbj
afntec (96)
cgveph (71)
isxysg (8)
cqlvkj (65)
ykitsi (63)
bjhxuqc (53)
uahcz (50)
vbejzc (38)
cujuai (157) -> ldqyt, fgcajp
kgphv (246) -> wvvivob, sjoov
gqqsfsr (78) -> ckudisa, kwhgtnk
dofou (44)
uijffti (65)
nkzgse (7)
xrtzs (96) -> ictsk, invra, rkgoovn, gzocw, hbptt, ucilvib, cnffplo
zvbqni (105) -> qkzpcyy, qsrxouy
zqbpucs (568) -> dfmjy, fnnykbh, gqeiy, fifams
lnndami (68)
rcluk (156) -> fjticim, mbbuqoi
zylotoh (7)
phcqntb (76)
amcixd (29)
jlsryou (95)
rceksnv (57)
vwnvlh (1534) -> krnbkr, egizyey, empixc, lzojty, vyinb, zeolg, qxnvoh
chyun (94)
hrjlzxr (1124) -> ybixf, qmshniw, zrnlkl
bwlit (20)
grxdbx (34)
eqgvf (9) -> wdbmakv, zqamk, hvecp, rbzmniw, jfofam, tigvdj
uqals (78) -> ropdc, pbdafpz, mgqil, pygrhr, eourjv
veuzidj (49) -> kmhug, yrolq, csfzl
eovobtl (149) -> hzuyh, iftcnc
udgssh (5)
nxbec (96)
dyomhm (40) -> npxexv, xbccghy, jncjxue, nadbegy
fsdoohr (64)
lcrbt (29)
rarkt (1387) -> mjqzvuu, nzhbfc, veuzidj
temju (53) -> qpzszp, cqlvkj
nrgxdmx (1352) -> mlalrpt, nqfqgj, cuscn, ogowst, vfrylx
tlpfggk (25)
wzimze (4005) -> uwjkolt, jynmg, ibqmynm
ddgnyfo (79)
fyaxzl (18)
qpzszp (65)
eitdb (197) -> trvtkqj, kwnqw
fgsmzi (525) -> dzuqljn, ihvefep, uhrdeol, gdvuw
nevrtrs (83)
jyftx (76)
lcitl (1114) -> nlwakg, hwgwzv, girxc
kzjtb (78) -> gacseeh, lycfbo, aifpxqv
zacwh (5)
glcsinj (90) -> aemkbp, yxxgd, fhqhu, xquhgig, nxmhned, lqbmor, qmyfg
rswnjx (57)
cokvree (96)
ewgadin (39)
oqsdzpe (50)
yznmd (62) -> uvkwg, mncivb
lpdhdvy (65)
kmhug (64)
bvtgvu (94)
hycgb (237) -> zmciyk, ysbhsn, vxslvq
ycuuwxa (161) -> vehziv, rvqopbu
mieqogh (19)
xadcuw (1626) -> chgfb, stofi
supjxi (21)
rfqddq (53)
qbaosu (314) -> hshafy, ukgbdva
ptxfk (73)
lofybs (89)
agjpn (8265) -> ocgbpez, ejdxula, kgbzu
mgbgb (334) -> kksizqf, dofou
izxpf (39) -> ewobo, ntezb, sbmqxu, tlpfggk
znagag (89)
zmciyk (19) -> mieuhe, psjec
dhobx (95)
hvecp (90) -> hrrld, jazqb, fonrb, dcpzwz, vqinar, bxqqvue
sjbfmfa (80) -> fewgct, jupzgjm
fmpvv (74)
bcadp (91)
tmzxl (23)";

            // example
            //            input = @"pbga (66)
            //xhth (57)
            //ebii (61)
            //havc (66)
            //ktlj (57)
            //fwft (72) -> ktlj, cntj, xhth
            //qoyq (66)
            //padx (45) -> pbga, havc, qoyq
            //tknk (41) -> ugml, padx, fwft
            //jptl (61)
            //ugml (68) -> gyxo, ebii, jptl
            //gyxo (61)
            //cntj (57)";

            var rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            var nodesMap = rows.Select(x =>
            {
                var m = Regex.Match(x, @"(?<name>[a-z]+)\s\((?<weight>[0-9]+)\)(\s->\s(?<sub>.+)|$)");
                Debug.Assert(m.Success);
                var name = m.Groups["name"].Value;
                var rawWeight = m.Groups["weight"].Value;
                var weight = int.Parse(rawWeight);
                var rawSub = m.Groups["sub"].Value;
                var sub = rawSub.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                return new Node { Name = name, Weight = weight, ChildrenNames = sub };
            }).ToDictionary(x => x.Name);

            foreach (var node in nodesMap)
            {
                foreach (var childName in node.Value.ChildrenNames)
                {
                    Debug.Assert(nodesMap[childName].Parent == null);
                    nodesMap[childName].Parent = node.Value;
                    node.Value.Children.Add(nodesMap[childName]);
                }
            }

            var root = nodesMap.Values.Single(x => x.Parent == null);

            var unbalanced = new List<Node>();

            int ComputeAndAssignChildrenWeight(Node node, int level = 0)
            {
                node.Level = level;
                if (node.Children.Any())
                {
                    var childrenWeights = node.Children.Select(x => ComputeAndAssignChildrenWeight(x, level + 1)).ToArray();
                    if (childrenWeights.GroupBy(x => x).Count() != 1)
                    {
                        unbalanced.Add(node);
                    }
                    var childrenWeight = childrenWeights.Sum(x => x);
                    var totalWeight = childrenWeight + node.Weight;

                    node.TotalWeight = totalWeight;
                    node.ChildrenWeights = childrenWeights;
                    return totalWeight;
                }
                else
                {
                    node.TotalWeight = node.Weight;
                    return node.Weight;
                }
            }

            ComputeAndAssignChildrenWeight(root);

            var firstUnbalanced = unbalanced.First();

            var differences = firstUnbalanced.Children.GroupBy(x => x.TotalWeight).ToList();
            Debug.Assert(differences.Count() == 2);

            var ordered = differences.OrderBy(x => x.Count()).ToList();
            var wrongWeight = ordered[0];
            var difference = ordered[0].Key - ordered[1].Key;
            var correctedWeight = wrongWeight.Single().Weight - difference;
        }

        private class Node
        {
            public string Name { get; set; }
            public int Weight { get; set; }
            public int TotalWeight { get; set; }
            public int[] ChildrenWeights { get; set; } = new int[0];
            public string[] ChildrenNames { get; set; }
            public List<Node> Children { get; set; } = new List<Node>();
            public Node Parent { get; set; }
            public int Level { get; set; }
        }

        private static void Day6()
        {
            var input = "14	0	15	12	11	11	3	5	1	6	8	4	9	1	8	4";
            var banks = input.Split(new char[] { '\t' }).Select(x => int.Parse(x)).ToList();
            //banks = new List<int> { 0, 2, 7, 0 }; // example

            var previousStates = new List<List<int>>();
            int count = 0;
            do
            {
                previousStates.Add(banks.ToList());

                var (biggestBankIndex, biggestBankValue) = GetMax(banks);
                banks[biggestBankIndex] = 0;

                for (int i = GetNextBank(biggestBankIndex), value = biggestBankValue; value > 0; i = GetNextBank(i), value--)
                {
                    banks[i]++;
                }

                count++;

            } while (!previousStates.Any(x => x.SequenceEqual(banks)));

            var firstSeen = previousStates.Select((x, i) => (x, i)).First(x => x.x.SequenceEqual(banks));
            var loopSize = previousStates.Count - firstSeen.i;

            int GetNextBank(int current) => (current + 1) % banks.Count;
        }

        private static (int MaxIndex, int MaxValue) GetMax(IList<int> list)
        {
            int maxIndex = -1;
            int maxValue = int.MinValue;
            for (int i = 0; i < list.Count; i++)
            {
                if ((maxIndex < 0) || (list[i] > maxValue))
                {
                    maxValue = list[i];
                    maxIndex = i;
                }
            }
            return (maxIndex, maxValue);
        }

        private static void Day5()
        {
            var input = @"2
1
-1
-2
0
-1
1
-1
-7
-6
1
-4
-1
-12
-7
-3
-12
-5
-6
-13
-7
-17
-13
-11
-3
-7
-3
-2
-6
-27
-20
-15
-23
-23
-33
0
-10
-35
-29
-6
-10
-5
-20
-38
-30
-38
-12
-23
1
-4
-48
-45
-1
-30
-38
-27
-23
-53
-36
0
-3
-45
-32
-39
-32
-46
-23
-40
-10
-54
-38
-37
-44
1
-56
-11
-74
-41
-73
-34
-31
-42
-49
-75
-8
-48
-49
-82
-21
-58
-40
-75
-66
-31
-34
-35
-52
-23
-56
-58
-60
-18
-34
-50
-27
-1
-3
-6
-70
-93
-36
-15
-1
-51
0
-110
-7
-7
-56
-14
-66
-93
-56
-100
-19
-54
-79
-81
-19
-112
-13
-24
-40
-90
-8
-10
-14
-27
-62
-45
-137
-53
-53
-89
-48
-86
-139
-91
-146
-109
-52
-6
-32
-6
-113
-78
-12
-4
-113
-42
-145
-23
-64
-97
-98
-77
-155
-133
-65
-64
-59
-164
-155
-27
-65
-57
-133
-140
-95
-104
-46
-16
-139
-55
-15
-26
-63
-141
-93
-146
-51
-104
-84
-82
-87
-149
-19
-77
-154
-118
-96
-117
-96
-140
-47
-188
-158
-141
-192
-63
-58
-191
-63
-52
-135
-142
-109
-42
-134
-4
-11
-135
-13
-24
-39
-4
-183
-158
-25
-136
-35
-49
-54
-78
-18
-92
-19
-142
-40
-237
-119
-147
-198
-132
-73
-238
-106
-82
-51
-72
-9
-44
-151
-164
-35
-74
-252
-219
-40
-154
-229
-169
-130
-238
-64
-171
-174
-161
-67
-205
-160
-112
-191
1
-60
-147
0
-43
-67
-190
-256
-66
-189
-76
-86
-91
-243
-10
-142
-163
-52
-112
-162
-169
-269
-98
-188
-282
-212
-286
-28
-33
-6
-114
-89
-237
-90
-95
-202
-266
-72
-215
-50
-52
-78
-286
-32
-235
-7
-56
-194
-6
-32
-73
-48
-77
-69
-43
-279
-236
-79
-286
-105
-295
-61
-320
-130
-99
-90
-238
-294
-120
-9
-302
-327
-165
-267
-228
-250
-153
-28
-126
-187
-138
-163
-140
-26
-217
-197
-180
-338
-39
-71
-6
-56
-151
-272
-276
-246
-189
-183
-38
-249
0
-185
-8
-193
-213
-296
-3
-340
-76
-97
-87
-1
-172
-235
-38
-274
-169
-70
-162
-320
-78
-222
-69
-222
-219
-213
-313
-179
-182
-253
-135
-206
-54
-167
-101
-397
-367
-54
-143
-147
-156
-293
-144
-47
-254
-169
-307
-223
-339
-398
-414
-23
-107
-235
-302
-321
-111
-167
-345
-55
-64
-315
-266
-191
-265
-248
-426
-47
-409
-212
-212
-401
-87
-389
-146
-97
-65
-286
-447
-168
-26
-371
-153
-297
-285
-164
-215
-336
-14
-416
-278
-233
-234
-392
-113
-80
-237
-342
-85
0
-145
-75
-101
-88
-292
-285
-344
-254
-47
-310
-227
-60
-320
-102
-364
-131
-338
-17
-239
-124
-266
-380
-421
-217
-311
-287
-233
-223
-242
-16
-326
-407
-482
-470
-247
-365
-75
-278
-44
-404
-195
-348
-81
-309
-181
-176
-97
-274
-204
-485
-458
-364
-22
-89
-448
-235
-53
-50
-510
-89
-114
-158
-199
-189
-204
-528
-278
-274
-149
-208
-485
-313
-325
-246
-173
-478
-164
-153
-76
-407
-447
-109
-334
-199
-50
-361
-449
-338
-409
-66
-282
-510
-288
-380
-562
-543
-534
-500
-288
-526
-439
-142
-284
-421
-30
-243
-185
-433
-326
-102
-540
-391
-197
-580
-305
-436
-559
2
-30
-204
-97
-204
-207
-79
-329
-157
-284
-581
-182
-458
-232
-111
-352
-601
0
-245
-292
-167
-549
-456
-277
-63
-104
-493
-585
-369
-121
-122
-180
-466
-509
-405
-53
-555
-454
-549
-486
-80
-463
-385
-538
-274
-75
-90
-500
-434
-167
-142
-587
-92
-182
-95
-205
-49
-574
-352
-638
-204
-25
-375
-456
-400
-572
-37
-151
-81
2
-19
-579
-106
-344
-339
-188
-517
-12
-403
-623
-619
-429
-53
-227
-11
-548
-426
-115
-481
-425
-9
-43
-209
-145
-168
-241
-331
-521
-77
-642
-397
-37
-98
-333
-281
-162
-361
-119
-696
-440
-663
-347
-295
-692
-32
-331
-623
-275
-646
-517
-16
-193
-537
-403
-75
-607
-74
-393
-333
-665
-448
-419
-119
-213
-635
-668
-178
-46
-175
-537
-160
-467
-271
-594
-240
-262
-666
-205
-48
-319
-738
-240
-697
-685
-711
-98
-134
-28
-731
-317
-319
-288
-236
-425
-401
-625
-638
-496
-23
-751
-643
-382
-717
-269
-275
-764
-672
-758
-605
-530
-244
-526
-357
-175
-667
-282
-551
-642
-83
-116
-751
-381
-447
-266
-297
-88
-575
-246
-189
-662
-450
-91
-471
-209
-609
-151
-630
-345
-625
-743
-377
-789
-56
-370
-250
-661
-792
-560
-585
-231
-673
-725
-194
-317
-455
-234
-282
-516
-784
-2
-652
-427
-31
-755
-527
-725
-47
-606
-210
-172
-773
-819
-636
-348
-376
-700
-727
-156
-574
-414
-34
-439
-413
-604
-648
-381
-529
-82
-736
-816
-595
-352
-417
-836
-691
-660
-464
-314
-748
-698
-49
-97
-721
-294
-441
-446
-415
-187
-212
-506
-550
-131
-231
-637
-334
-853
-383
-407
-219
-518
-743
-83
-773
-162
-570
-611
-574
-355
-56
-775
-663
-131
-357
-560
-335
-390
-667
-516
-897
-752
-786
-246
-893
-693
-692
-647
-422
-361
-148
-231
-775
-62
-404
-783
-387
-559
-703
-403
-776
-588
-633
-831
-779
-23
-216
-381
-287
-517
-402
-814
-756
-646
-535
-270
-282
-157
-367
-356
-925
-333
-375
-469
-931
-347
-455
-942
-815
-311
-690
-65
-691
-64
-361
-409
-886
-488
-303
-806
-73
-653
-356
-71
-523
-370
-685
-526
-528
-519
-179
-762
-652
-388
-568
-296
-601
-822
-656
-258
-304
-670
-731
-352
-82
0
-116
-294
-652
-702
-933
-12
-348
-15
-662
-311
-695
-357
-872
-847
-791
-129
-574
-281
-42
-626
-36
-60
-864
-871
-246
-943
-500
-253
-684
-545
-1011
-330
-666
-468
-780
-596
-872
-812
-924
-836
-379
-528
-464
-99
-675
-317
-58
-641
-590
-227
-296
-303
-798
-39
-824
-300
-469
-251
-182
-40
-115
-997
-572
-743
-13
-557
-542
-832
-884
-385
-224
-932
-757
-405
-690
-745
-1008
-657
-846
-565
-508
-792
-245
-298
-793
-278";
            var rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            var list = rows.Select(x => int.Parse(x)).ToList();

            //list = new List<int> { 0, 3, 0, 1, -3 }; // example

            int count = 0;
            int position = 0;
            while (position < list.Count)
            {
                var offset = list[position];
                if (offset >= 3)
                {
                    list[position]--;
                }
                else
                {
                    list[position]++;
                }
                position += offset;

                count++;
            }
        }

        private static void Day4()
        {
            var input = @"pphsv ojtou brvhsj cer ntfhlra udeh ccgtyzc zoyzmh jum lugbnk
vxjnf fzqitnj uyfck blnl impo kxoow nngd worcm bdesehw
caibh nfuk kfnu llfdbz uxjty yxjut jcea
qiho qif eupwww avyglnj nxzotsu hio lws
xjty usocjsh pivk qnknunc yjcgh bwya djw zpyr
ycfmfe mgq sjiomg nfzjul bjwkmgu yvsnvgj dcjupu wzz blmn
rdowgbt vpwfdoi blzl laghnk gsa vhnpo cztxzlb rtz hvwonhb eciju pfjtbo
bqs bqs dbutvgf mmzb izpyud rap izpyud xlzeb mnj hjncs
xpu vwp nujcos piu irindir tpmfd umtvlm gznu
sfpuxar qcnbte omouazv cnh uaxspfr sepolf rusafpx
xbmaf iceyqqq sabpt gliexel muubepe qqiyqce fmrcc eazk obkeonl fmccr kgk
apg gbycwe gap pag
gagv saqbk lwtllc wnhzz khxsjc
lgc alen rlmsp anel gcbvg
bujlaz rks rlqf deknmee yrp
scqvl weusbc bgvaz vgg cjwsfno vqy zbq aqy tvf bgzav
hbki vei fxdwljs myjuba elbsib pvy xxjxgi dtgv
linzaeu qbwdke fdg pykw
qvtdd aco aav bpu mvkcuc kjfj japgfki jfdl gem hog bdzsiea
wpbigkb lzhwba jssjkn qvb kmwu qddv
iny osyvqnt tumunzb torq bdeneg wywank poza ipp iggorw
tuko mhdbsf vmjdop jomaqpj rcdsud hmgspr lsas nzmwc
cirkjq nmjuu xtgejv gtexvj vjcmtqq unjmu
xsdmezq xvqjvqp exhygy qahju hvd qadmdh lok
wvvys kax rohrrar rwhnvi lhnmefp lsktouy bxilosp
wayf diobnl zvu obnidl oibnld
cewil ygsf ffzp ruxhu vah lnvwt aef lnnjc kgkb gxtlx feko
uti epphrin pywths cpzzh csjei nczhamy gayxmb bdcytq xkx fgmt
qvzyuwi dwo swkw bwjdrn dasgd ijgw vzabaop yefyhmc wgij
dyg sugrf vid etz weyqg nyntx dwfgwm khon hnzzzn xfyra
ofbh bdrsk rdrjj elaxvk jrjdr
msxau rsocvx zxdda mxz lknl
qktaywx dirpdbf unqnd wbrwkuu fvmqwl emxr big
xwz kvsydc ayokjyy qiah omw neo htltxx fxhwqwj colqvbb sxmo ephfkex
ncjxoaf fwjkc czmhv ylg axcjofn dvj bzqjku opvcr jiwzucg vmhzc
gmmnrt zqar twdwrg qiwwki fcbr lixm hjdwwe moiva
roinlxg cxeezve whannk cxeezve pyoj boweioy cpkgxsz
qkct qso xlb xyy aellfet rzt cbboow devfb nih fhbfxzi
qyc ltxia alixt atilx xtgrv
svruz ufvo rvesnxv dik vzurs jjg idk
xeudhrg hudn cilo ljplosb
kpb oyzvywx vldko qhfkwod bkeutk zqcqug pbriu wqocos
qkngzfy whobyri aze jvipdty ocirbep icqwc
kzxxlab sjr zhymws xkbx
nnxs gkwtld dwhkry snuibq dtdl aicug bhtlfzp qzk jctos
regvro mxcq hqof yraucxi jhkol iuxineo pbtnk rfjwc szgjpr ndqqj vfgm
yqrfox xoqrfy utbryu utubyr
jdubjt wqrl wnk rlqw nwiq pnbn qinw uaff ftdo htfrav
rum mur umr tij ovbahl losao imawwpb wadhww tbteyqc
napxd kzeiqcp ppgqucm xkityt frq hugrp gjgtt gmuqppc zwqme
xyuzs ysch howlzgu dkqppbs nvbiz mks mtxv vivouex uvawq
ffe lfsn nlq mpulheq ikcfo wdtz cnwsbph zkib muu
bqkxav wtecb lxwdhr kqbavx aqxvbk
czwswqx ldkxapd pfwd bdkkj iqohla cwosw ihqpd pcc ckhabbn
foiip hau rbqiyhh htm omeubgh symh evfcqg
lqx xlq rsgf izu esetis
npsrkdj fvulgkw eovw mzr uobcze azb tij ihoer ehori jit wknsqhm
gnrksh xwggt oosi bpnmhx qqaa mpmryu jhzyz
yad gbexqcr gbexqcr gbexqcr
ldca xxhznn twyy ytwy zhxnnx xfmpi
floioot kfyh dhibv ezyznar sfg sfg ezyznar
cinioim iiocmin ypla aypl
mhwcjbz dftuqsy wswop eizbf ptsd
ehx mlh nfxgfkz uuw xftmn ptlkbo vsnyo ttwce
oexvf orcg cncnkfk comvhl
lqewsj lyulrcl efixd qvd fhznqnz yvrkwyi xmhgc vzbp
dmr wrxqh thcm giomp rtvl ssc gwq rbklw hcmt fjvud
teozhb dmzwfv qkq pvcqfqq
hvlebc qqmg repxk zwrjdx ztruwb such tyligs ybg
psa rqznokd lgc jstqres yiqt mbiody xazb xjuk dtb
lea ncm rnh myzqzwm
wjml eums ueflvbr cjpgnl qduunu zfxaai jwlm lprzzg vrn ttetyr sume
uwkgeu uiahd plyewgi vveo nwhsitz mcitc uvk zsxehgs sewl
lnbdrka sgtivn sozzq mgd vhxfnlr twrfpk
gadphmk mbx lmlbrf tsnehnr lawdpm fnima gxgl
umty vrn dpow fsnnpjv fsnvnjp nnsvpjf cioaio
euu uue zeskmtk hob stekkzm
ypqpri qwdju ypriqp iprqyp jnoxqa
lkppi ingfxw wlulvp yhwrli nxwigf oyuhq ggfslx
kdd ypvr pyvr waw vyrp khqq mamxca bapq gobfm
iuq upvdpv zxef bfwns lmq lxswr kpsqo pwde iaaou nsw udy
lgzo nil ovgrmt omgtrv jrqp pqrj lit
uumyu iiakfj gvdtzz qbux yxn ejs dvlts
hcm ghutxq zswi tmyhqef hgxtuq
shkhkdk kad seubeax kdl mzu
cpykgr skx rfhpor xsk moyhlai ogv ophfrr dxipuuh
beyw jvrre opodn zdoajhx fhg ijs drczy drczy hjungq
jrzieja gfg yzdn yxm wshibsn fgg
xtylh vxscmvp rfymq uzhpyea spxcmvv dlni msj yxhlt
eov awql miv miv eov
mmvrfbg fjiyf hvqz zpuqmbf fszyuz ldfgni wemfjl fjjpl rbnpy rfb
ppzpeh nam ntv xnchtyk hja hpepzp foj bibvx nmmdlff bsrkp
qiy qiy umhlnh qiy
tyds oepk wae tdsy sdty
ukawr rkwau ghtjhm axy
wtbjiv btjivw ewaf hwk ttq
kdpun myve sqv rhvpy fnjwt puw ujhf thsp nkdadqr
vyw wkkpdpy xlgz lmmcuve ncuq lmotk
pmsfw vxd jpe qxlyasx ejp gwuv
pmgyndm ezofbvx nicbwrw kwnlj yjvnas fdpkfo mqcsyhn pyjpf fbexvzo vkftm erl
trmwvk rywuzoz hbidea kicohfz heidab deaibh
sogf govd dknpk vxrvk rlm vwhjk
xnxbfmw wguzrhd zbmkz piwppa mkbzz xvwrdgy flusfqb
cgduq hbnwr xfx mrejb ckw zkbaihf cloow cwk wuvthv iwqctx
vugx qbucd gxuv ocb cob
ilmet fbelxxz qratdfn unoj hbc duv srmikz
vnzuw zgpbqgf uzm thysyxd dinfh bgvr olungg ksd dsetwqz hpg
omagsf zpr coa kknx bzithq pewp flvoz xiiq weojqr wpep
aagj gcglqt gqcglt xbfx dhdx lbx
pljq plxuscw ilh wfk lhi hli fouieyw
hvnh zvm aqy dzitirm veq ctux
lglhs aqibdii hjbn cfgc qrg pnbntcx owoks ebz
jozngde lwne mbo omb fnyzvvj gndozje
bbdgc igtdj uhahgp sqduko
uuspedu fgnspm ewc slly jbs chl heanm abqijx kadvgxu
akfsft skna kusjqr rkqujs
erc vrljpu lruvjp lpvjur
iors hcdr fsqtcj vop vmn dtqnz tov oscjlw cdrh ctfjsq lrnts
fxp mczo sjlcxa mzoc jmsq hcxybow dmrr bcoxhyw
aac ewraerq odmxpz aac aac
zzio zebmxa szeej poordr gmi owwnnh xfx rzrab lfey jesze
akc yyoj vqod drtne
joxhvyf ymasnbr omouvq isxdrr
qyi ayrkzu jsk vqvvno jkkuxi zufnnwu mrsszdf
ocqi htfb tzjna cdt wkzhynm eergf
yokzugl usyuqu qvotq uweqyow lygkzuo kpmqmb uglyzok
glvshl imqv jrv xlpnsy gcg psj irtiamg wkl
bjcpc nvyloa dkkan efj okubpc cxlowm eone kmpny
cyxqys nmuaftv gqxj gtvsc
beouh dioxiah kizdy hyi cozrray rave fqxmxmj gdm
frjz amrsat lxvhzj azhevtu vxlzhj
zwmnrk sbk txzrcsj sbk oosgfej cvh zuthibi onvwd sbk nhwpzq
gzamt vraw kuk ugayl lyaug bww rwav ijah
bdjirxg vifjr rhbxpa oao yrhjxoi pbn
navb umesiys yhix phuhu aekkciu nlnsiq wjf idqdwp
cmhw rsu urs ziprlfe
kyhxitv cgty bnwjyq cygt sgjn pdab imarvhg yjbnqw
axaa ejancv yau njpc jvwy bpft kwjvg qzrbvtm diu njpc bpft
ambj upe rmqr yudbiqf krudp pqyf
tnb mobnpv vep ohxoc cyip wxyccfo jrbi rwsws kls zlv oohxc
fjh dmb hlbq bqc jhf kax suz fjjg rkpc
wjnn byfirm goeyh xtjmdka
tgyfxx hefpxln mveobqr yeo ftfn srt vim vlcu hevoi xtaaff
imyql xotcl poql rlueapq bkwykm hlalk bkwykm
gkec zff hbmtq rjxjbcf arerlu pvz cdaqi nijmhv uodwjh
mpctof mopftc ksfbat sbkatf
nvdd jub bvi kyggdbx nwtiok gjt mgsm dbhsn rzibgjm dvdn eqi
ysd iirp dfgzza wiyeoou ysd ispkv bcqg wwzqgq xphse
ntq ivposb gsd ezl tlkztp lez qyurp vxsmg dgs
wijs rydbj onm usiyqzb hwrol giusanb kewukl yziuqbs doojam nom
lfacyy xwwast truqtt tzneimn uxsydc ktu eqyaj ndszak
ffleooc kikif fohgop aucy moubqxu
iaxc pnwexdl ncy vmwm xrqoi wpgftq rofx utyzjuf stdxq twpgfq
ppmlp etsvi cjdx poly ynx vfxpslg mqjo qnpsage flpsxvg jwsxiqt
lbyhnb kflrpeq ssoti webxr embbjd kbnx ubzqco
khhc vwuqzb ebocbko rwmonkz edfqn hzh qhncoq gbwdi wjeg ocwow
ghzhd kcxblp lzwkkr gzhdh umk pblcxk
wyajtw jiff ouylv sni lwhlrg avqjiis igzx wbl lhrwgl
glhh kaxha tqii hwzx rgic kaxha rgyidmt qdgxfl ynjc oibfij
bapj bix rjniw ynbql idlvnmt wynpzbl zlpuoix kvn kakwys
aldpxxu iojxp rif xbyqtr jffdvy qnrq tqwsdiu
ulssco ktbymjw bfj zhkg zgc ctyri
ilrmq wfahcgk mrlqi bguad inj
cjzc rekuy ifr wfkg sple
cvjkp qbmumnp mprg ltmwxxh zpemtyb ozzssfd ksu mgrp
nvc sxp mpkxz bhlctq hguaa yrdkm iwsgfg qjssh gobbies hucdh
jdxrjw qmo qmo vobhnu
dnjib wtjp rfdjqdj skpvrb vkwevb kxxovp
fzi kicta zkuvr rfaawv ehklq cfdjsyb tukahwr zkuvr kicta ouq
aba ytdguk gqmpn hvxabff hvxabff dckj
fna wxyqhxd hvy khsu yypoyy lvvue medheua gim slf drdbeh ikihf
jquz wwo wwo ghlz jrbvb jrbvb
jwzvkl yjw ouwla yjw ouwla
zsvlgyf rzqbtj qygynem ukdgjm lbsyh tmdzp fbcaim eymzr
pvw sbs dvsa plmepl pwv ayxk vpw dwt
inayadn pnti yzhxk azga gxq aznbciu gjnmyqm
isgf ndqmk beyqq ebyqq srtzxo aiiw oqfuwp uoqwfp buejctv pxbk
pzl irv tzvzdb wcy eszm ybwiw ycw riizifd iybww
btpu cua azzqffy owcr
ofwq sqlpzat lozdxlc aevjmpc lcolzxd wbbysn qwfo vcrx gdzgi
dbpfmxu ydsxwl ijn svxtop csep ldqeog ffye zcrl soh aclw
wyiyyhv vyhiywy obgi hiyywvy
ddvaoc lhv spurn rgxyy onjw illvn yryxg xyyrg
vid wdttqq kajr myip
wolqlue phlunpt dcmmkfm sgxk dmmckmf sfng jlbsntq dxp
zmneyho fswj xdgsjc oefwjdi htgxvbd tgqrq xodoa
ynw bygqdnh hhmnkuw cojqrke qszzdjo orskwq mdfae asabn
vvpm vkj pcxghao caoxphg axhblxb vvmp
txox nzy eqn zgir dytsi girz ffa ugjjbzj brob fll
kbz pukqbd fiwmuh umwihf bkz dvz
vgs vejs vejs vejs mbkyjjy
viqmnmu bitkyw nddnk dknnd cldnpp hipub plcdpn fdzzpb mmyomn
ndylnfx gozlrx ngptk rnpteb wtacx xmtcjy xldha
fey doyxis ampmtr ycqh syw cqhlj hnngx
dijf nac tvkq ayo akbj lzmngdm wfxpn bpyvrf cvdqpa
zsofz lhho hgat wqskga mnt
mylwm zxsd omzpa waz hcrr lxmpq jsw sqtwak pzoma
rwhgsgt ysdq ztihici mpwcawv alkqg wsxiwx
snldn bcb anjdv cbb awsscc cqxult hjmjew mcycb fdpdg sesrh
kukrqm fawafz qdim wyobtqx bnvjnqg dcvqxta yptr nnpu ughldqp duo zafwaf
knb yjqb bscpnt nzg sqeu zkahna ttuf nsbtpc ixwit vucwj idix
bfqyx xlnpc ijrxu zkqi kjxtahr fgag orusms adi bfqyx bfqyx
dqddc ncbv bvfk hefikb dqddc hqjl otpx zfiu
ntkv qunrzx eztzure ctt rjo bkdt znvd jwdf gqhf mmhrzgt
zeavm hkbf rawqwuf pis dojlkt vnjhmi uvk cufmn qginezd xyut
hnidzk chlctc yst pepd dxntbxg vqk daxfpmu wshyddl
jgd vesqgo bdyqy igl ahstdm wjtd lrtkjsv tjsj sccxbih esn gkkzj
iisiswh jll rhlaf jqwwgfa wmhyo izva vrg zjkak nlxxfer rvhx
mkrkd jlqtpy ukstro ktuors wsj ynqpbp kpiyxzv nxeiwg xpzvkiy
jbr gnct fwklekg cmfqnm ctn gqobrs kwht
pztmjs yiffc kfhsblx yiffc yiffc
biezil iiezbl bzeiil smocoju
viiigm gmmmk yeiv dxzogro qsmzsur hukzwjn lcle syo mdj uruf rxfseu
extchsd adeff ouikoj fyaclr rwwvqsd dooe tcxheds zrdqqhm fdoxv kbxi tlcj
aycnydq qlxhka zoi shplo qll
bfry lbwckm ltq rbfy gpn vojp ruj dpxcve geq
svtvfwh lca lac qia vhwsftv nookdfz xgjiaf yvcdlt
aspgqym fryuzhx bbydf tbn bwutsc fqgi zij lmxhog qnmse
rbb gsys volnas onvlas lonasv vwjdso lnteapy
got iauk kficn jvfuy yvoe jcxwui hyamqx mke mwh jcxwui hyamqx
avutfi ggmha dkopc kothnnb syoi xsd wjedywy
oziejyz yzeijoz hnthyn knj juuq qujtp kgq bymlnlf yicf
zsejuy dybeap hvowmvn okxb yoi epadby cnzjk xfwprzc
lacg iiix fblhxvf nrkkol lnafzw qspzsn gvdy ipj zub uouseo
evukwkh ycjxxc lptwmf pmd izxdsos zrkavf pgjoy zwokg mpjiej
vqw ijwoy eaw wvq svmcq ccxi nyub ynlq eqornax uprt pygfe
plue okbbm btvm gba kutn jacjx ysqt lvx pcxxu qcf
pyw ffjfudq bvk hsdwdva fjnivhf odbmw krpgrj
hziesm bxa dceiwt tmvivjk snl fkh dahsxyx kqlhak lurtk
xss sswyxrg yqff dbkx kbxd mpzbmnl bzplnmm
uvz pjm ilrol pmj uzct ztcu brhkv
heiz jcn syjt zfvlvaq aflvqvz amcjh rxnitw
cxl nxvrn vjnz aewtr cxtko nnvcp ltptd adpxt zvjn fntklj
aymmm tuirj hzngq zhbh paqs kvpfo aqsp kmo acprw sabrso kdqmp
ndqjspv mmhp pndjsvq rti usm
ije oad mvelyg jadz ekm dao zdcmv
qwww tmwmdbb oxxfoza rgmf eonku brh gcgiuoi ojscn
fjedeek ohlax fiydku rbnxpg wfivg cdgs
axwbni hojye mwfe oyqknxp whdgfy ihku mbhr gagnz hehagxj
hibautd blnayq lnayqb gepml mgpel qunw
ircx oeb kujtip zbu ebo cmmn
upyqvot wbponp hnn vav avv tvrky omm
yzqsnf agbfsw dbxoya sfnqzy hqrxek qsnyzf oagyerm xxhukm
xzvk mvcwz oujr hell hoe xexa dqlpqt xdqz ucola hsvv tcmybhl
skldxr mzyol ybzyzd jnnxb rxncdy nkpwy fwlnsw omylz oiwieu fshv ngvha
jkwqf yxrox hejfoq orxyx
rijken xiwf mawqcfu erinjk jsi yyg mmu mdkfqb
ornjes krp eornjs enjros pyqp nnwwjl
wzd uqqo kyeli tikdle aykdjog uiz rbpnw mjxezf ihiz rlgyg
cjm ajqgvkz kfgyy dmczlc mjc kxcm zctyqgh ymsk jwhqfd czpqgan
vxkzvco owo qogj uyictoj kfr pyoo ejrru npluynx bvv jhhzu kuciwc
eqk pcsly kelu arzgoe trfo fotr cuaax
lagonw qvcssqz sdoklh uvovi sfrkmd hnvafj ltg wfjj
viwbkm hpwe kzzwrbr axjtlq mznin wwpjg unlwur
nuzorgo qfoz ydisca qxdfutv hzg
nqgge tobtt hjocx ntyqyi rxzkynw wrnxzyk ciscy trjt ottbt
yuii srawx gljxe eteogz kcu jlgxe tjik ktsnp agudqok jwol vfnyv
vgicg dhnrmxz sjhozy hlalx rutwq
nyoyoje kco hoyam hoyam tta iflud amh gdxcsj vqr fvsqcgv
xdmbtph ueen cskerl rxjvpdc
nricn addljzg obq rikez igq bxygkmv qmgojou uheubk qor
snzd ztusvr vrstzu mceddga hgu
vvrbfjg mcdhmsf ldtwl otuna gmjurrx jgrurxm rxmurjg yrioq
iotkvo sftfvn vvoit lllju xvlg rdsb ywmdf mzxigu kzq
sgqw gqsw lqfu wgqs xpiwou jurgucd azq wgaqpm
ijntzi chlnfj yjqatz hjflcn vys ofq oqf oadthe jrfw
mmc motjo vcwmod rpaszfk zgkkua bpja vjb htrk
bpfvvka kmger mnvvfl hakudy yfprdoo mvnlfv rgmek evnwg
mykpu juavkn cecdvi aszbi lxm hmps oaqoif
fshizd fsdzhi lvcq hhpb eavwno auqlwz rpv owcdojx amsmf qgnddd
pohmcn hlcxk qsesxh rncr
fgyrsis ldem avxmnh frpodq oefzn
plfpu qdyojz xdrzrjy kpv abkh fge bbnotvp liikmcu czvwl oyh
ovha muitw pzy edfjoo fhsxuh dliyruc dikcd cqem ywfy
exyry jtzqn tscr qbtxno cikk poqgr tnjzq eofe sxea anlikep kick
zcie purpw dmhhms bcdo prwup uprpw wfejgjd
kwtjc cmixp dodfwj hcgmmat pkeyspo ubnl ajxvj ffkh xvw
nvlgq oduus psufiqg lrwpn dleftn xtllqvf usgz
liarf sczsf sczsf wky qtzq qvve qvve
cit vtjsh jrhkyvi txj urmq hppx
rhblmxn rhblmxn lkgow dylurwc beyk gfcewxj ehpl disoe tjbjy lkgow
nbkrm jvk ffux ars agns bebic jzjfm kmnbr gptvtsa ufxf
hrlvup jaz tafyr qcgq wkd fiz bgsrx jmtcvo qkbvj
eontk djf tiafrng mtwat puainel nyjoh meynxbf eqdw
aspvmbx tgzuszm fpj xkl nzpr fjp vnomk byx sbtov tnu utn
ldyww gwmiddv hwyh gcgsdit gtgdisc suufl xsw dlwyw
sye dgbd wyf ixqzthx dgdb esy
nsdgera fqz xwbdgui ngdgbcd bcn qrdxml cwcmxws tncm mqsodj cqgk
estayas cocmbpv cdcf vygtswo aplwa estayas
ndc ndc wntr sfls sfls
gse svv esmi lcdii lnr kemrk gnk ildic blnqy wvn
mwlpm awkr sxsudub yauwww hnktbog fpnqc nmxoq yoparu tqjpkug nbipft
czwnkk hrodtmx yyzpil ooqjb cvxzfh
kwa wak gipak gsgrw
jyy fja jjk kuvoqdy urqx
doyu chgn gvtxi qjdigvy kxr dizwrjc sll zenl yyblj
epxeqih kfi hlog pakk kkiidrh hiufw wuhif baqzxzi bgcd phi jzjdxjp
hllhyad sodc nyrtfe kygof hyyqi txddqg wcwxvnt ewqmj wwv
vxymuoe caat diqwbo vfruxdf sqniefn hetcbl nvtttu ouesb
yvoez pvthzc tdowuci wjijicn fhpmq kfobag yctdwj
xaugkb rprkg tidpx pjk tpwwm pbcfhr wmwpt sfynrl iouaw zbnyu
auakc culuxg bffg rodyhea ixlmtfb jdurl szoa
xgona fjzho buh khbvti ddh mgj ptgaqps
dqldupd udpldqd poku gfgpcg zsvk grvk kntx jih uwvxdvq sivk
mwdnq wmqdn uzto mdqnw
alvfm qxqo thwru xqqo jilnsgs rnonk fwntuby ogbha
gvxlxyf cdpv khvpka kgt gshlaa tenb
mtgvvxh mrjrsd truk rrerzx tujweaz
ozepw gsqkr rtmmc cmrtm
spnthg xhlzuu xwcrxz aqqejhh bpzh
ectdftk rgp mkp vxp pevriz wkgfkaw vfygj peg gep wjn
bksbu ywsszf tsbrps vxicr hfustju ynnlbo
sio urbvf ujezjk vkyc ukjezj bvrfu qwwgqmw uqfekvx bzipxus qfumwh
druru kycweog ycmef rjyy fkgp
rmf ifbip rsztco coju wlr bfbmsug lwr bsufbgm nwmp
jjuxtyd yif rkldsvu binq spepa mfg aszm
ghilaau ncm sgbavz omzeotz azukf bgjw zqzo gjbw pld
gtog iqheik budeu guvljmi
qqlj jqql ttk xcxu
cfq cfq kpagib dxfxufw hhksbjh gpcp
xkeax acnia jjubfc mhot uxlhh gnkj pavta rciondm rkquh xudqian
wqhqzg psqh rnnc uujlgq
hpjpaoa maa rdndl xewqj nmagwx xewqj hxuyvou xziv rdndl fbxmbz hmfwghy
dtwnrca hbfcptw qrmvat sdatx les zwizogq
bodiwzg sgoas fsf wgkrn zgbdowi wfkz
ngcsg grtao wcfxpyl gngcs fxwycpl fkpt
txvngo vxngot tkoap zqjc qzcj oeruix myh ihgdfik qtt
rxeh fcbnoo rxeh lve wvoc pmnxej dlcbrh rztt noibg
zyvq lwxqu oyjv bvidmf wxuql
wzc zcw czw dnhkvrg nzslrf
cfgl uwhxu qnsfmt tgyabes mqnq nkitq hmcvxlt qqmn yzmb uomqp
lwziur hgmdmv zuvipkp vir apr gfaq zeo dunat mqgafzg
prq pqkr xlrw njf ncqni kgpoma cmtklv
jwfuc poz opz fuple
fgleub lcgnifu lkwo kftbc onvwvdx lukpod xgmh rnj
rwqvv ezjmoni llq ekd cdvv kzcci gzsj vuipv fnw
rtnua gbnzg kqtogns iozzwc kjpzz kiiurey yzlvzx cpy xrue
fexcjmw ebwssx ewbcgwd uwolou nfdhic vupiykn jss djoo xftbkgo
idf ipvmez qyevwd wfsjxja dif dig
szpbtsa bssaztp sptzasb qppgz odur cpmn wpmg
pxn zjmq rbnr azwstzm mln upaqyty nxp oge nlm
bfaryqv hag phtvh ypi
epeeog lip zqio wuehlnb bau sbd dsb
xbrrp sej agrqnpa aarpnqg bnwyi jbn
uqmsvd asmuyy czxviw pznnmvc
sddwmek wnaea iwphupk sabo
cingdks ksh mtyip zltgafm dflkcd wbdnqup uokm gmxpyd libz svv akce
qge ewv dkabkmb xcpi nrkmsu mkmb djvamg mhhrwjh
krjt etfhm bxzatw zdkvz ehov seyxbw mkiirs plzoplu sogmwb wodfcle
qwea adibdp emo homrd pjcrhlc eqaw kqsrp rphjlcc
gajzo nwjg qxjra jztcnir ijvjwez avxb afz zyywqz kcszgh elmlkfh
lbz ozia bctf bumoji anhil rta xvit
ejybire ypjl qevak fzalx mlh qxlei zib
xmzas kwojjz ntrnrw nbmxlv mdgxs xjhxg suo zdcrxl qkujisz pxmu
eezyd unrtm wyu vhufvto rpb isfcy ygh hgy
nszvbzv ebtt memrsva ebtt qwcaq bhbas pvzfbov ppjbdy nszvbzv jabvrp
rlo zbmi lugvu yeby
tfcd tvl faaq mnural nyarh xnxk ctdf bodz
vwdrhc gub bgu fpcovx rcvwhd jukwsue
aekrhi lpknnrh bett tkib ioqrap igwnst aekrhi lhha
acg mknhazp pcgjuk tajplv
masq fyjkn agq qhxbbl qga npzj fme xtihic rntisg iqv aqg
ipagh fjth mswztpi iexd cocojy vhqrla joe wrsrmw
njztu tsh auqrxca zpp
jctn webxi haq irrr qox irrr webxi
reaw axmnvd voakf lnz ftbxfh zjyxzl pryfjpv sistgb pov mshs
gsy ctsngl ptmnyx vpjx zpvtori pfu ioycdrq
aobdtlj osdnrth sgqe geqs qegs
oamrlxk ygbb rkamoxl nztl sarbmtj yqupjt plu sbtarmj vpa rxea
yvhgp yznko epwpza gqrsod rilukp cglhomj wnaplu ugvdko qdr
cggztg ajw gggzct ubmiefj kpa
rel lvasbh kobm mdnzla pwnyj ehep gzx nhjdnsg rxa
qaz gook rplqwh vsht
dhe aneq ivrn awekad ckcbt zsqca ehd rvni oulwfuu
oxgzzow wntz tkqaoi oxgzzow lwkdpgy lhd aekjasp tkqaoi dnhaw
alxghco cpanoa onjh hyeyebe whxn zfu zozbll gojn
zdqulsa dlqsazu zqudals sfedw
rydtrsv rrtvysd fvyza drdgh lsfzt blnxr cnxe tslzf iijyds ylcxn
cczea nxx kwol kopaza wuvr cyvoo whlicv
zbmrwdq tlzbevx jwzpsc uvkwpd bmss rbzblj
jogx jgi gji hypmtkg ijg oscjv
flkoqja kwmrqv wzehel fvmcfap mkwqvr ivwxg jqfwdvo hweezl
vgjg nzucho nuohcz ggvj tmxci
fqaqx zeybhtg bxeic lftuqp wzuerz sww qfltxk
keiy myrvp blkxcg lncqmsu diittlg fqrf digrel cpwrk ipan dkxb bymlzo
owm irygdz pyhj mow wmo
noul pbvvt zcv ueqyjl zhetlw lpjfhli
felvwb wdykz kyibdz haq qkouj vuav oztyqh
dyxo njcr hcuk ysrr pucw qbajztc
ooyaz pmt hqwu gjx tmp tpm pwz
lyhzajz dfot avyifo kdwka pwypcep kyyw tirlku zdpjmft
aexle hfxo dacwvcy xsiotyg cifq ibupshj aktt rzvf pgafj
pxubhw ibpm jxtxg iwnssf osbpj
exmtfyx blbfg emrunru zkuhoi lfzn zrj unmcece phuppi
icomb rmy mvsqqkh zwjubz lumq wekx
cmdgs gsr pfhqx pfhqx cmdgs pga
rpyf jejc adaiou dutv imbenyu dqw zhebjhu pryf vtxs yprf
cxj roprjn rqoh qacagru snxd
rczvi hfpl luc yowgj nvavlhw vjudkmv dwu teq
klwc cktzh ksnvswl nsgeu xyohp mhs fxnjhm fwrcg rdeadkx cim
ounvb vzqje ujctzzk iyy vxck ebtvbqr uswsmcr jveqz qejzv jmi pboq
lwffygh mqsh vnnj ufz qhms gqfuxo lurzmu
buf psdluck gapwoo wgll sbfavbc lljfvzx cdgo rpt sfvabcb
svefr kubbri fervs nboi zkvq
jwr vtc zkcpzb kczbzp cdned pzbzkc wigjuak fszgweu odflfek
vwdqm khnnj plokjg vnce venc vecn yzxtgb
tawl yrhoz tawl yrhoz
vvehsl kdhzgme rix rcs btm pxnlsps vlhesv sxpnslp yqjtool
eqpyw kpmkcyw wqhglxg ajfzo hbd qvmhy nhokah iisqvad kxuyd fxek
jsz txhwhah hxt djnvl srylveu pxp dzmmn epek tzs
joyzql jqczueb rtdyw fyc fjirfyn tjcalz joyzql fyc
pjrmiz xwnmwns kcqjuut zfgxhdr octwn kqppg zhfgxrd wmwnnxs
ema yqxqs aljjo ajloj wozb
urgmhiz epqj vhhaxdm ptlsvig qzbmm cumbho lkg gyzmg eaopyzf ncfy mqe
ijvwvo oszkees ugvyk hjdj ftip itfp
ylfw qutzdj mgqp cyjss yzsdqqi iykvs fyor sthyqp mrjtzee hgo zwqbtgk
bkfkns gco bykzc mje dwmkrwt ljegqor yxjxp oaleuu
xeltq ggyqis aud frtyxhx iwz wiz fwoxz fozxw
zdu nwduqsa nced iphaaxo
bqjj oah ezd brhgxrc pmkz kdog exw
ihatt hck iepn egemprp wrz wzcuo xjzeaa wku ivjvihh
cwkuof bmj qmxd qbtms zgdei bsqmt ssndhw eeenku lcsqy bvvodr
tek zsgytci vgoun kwwu
jcxvp ijxc buqgix uil zfoku
ggndshq bmjeo yqaxtik blspz yofh edaroy
ipvtxh ouye elln dllvx iqza nhwf zyfw pvlky
iydcx gvarm gvarm wegmiy
sfjd liiflle mulboe qywzs tzbns trojl pad mnfcrhb sltb
gthqj jvpsof jwlfyeg jwhlfj
qckv umzrge gnzc mnr xde
gvgxmhv txnait taxint ius iboqdj
vsfex kbpvsby qembkb efxvs vhflzvm eaazg dyg bbmekq
wxpfk xwfpk xwkpf cjsyi
knzg eefq feqe seppop ttxz qnqfn atgsy cch mkjlbwt uyhct
quzw jbiw miqehe qvf jyipqh kzcjxyh
teuvzf tdtwoi pcuafa cwgjk ccur lgmqv jpjdkk efrnw uloqn dpkjkj lwloeph
yaffjy xntstsv gygq sxttvsn tvnstxs
cvbmdf pfrfkna wupv van iocb hsiyke obspj ytyfkl hbsqtij hkcw
oeddmnu koso mdodeun ybe mhjbmwy ubejz soko yxvuv
nylhy ylnyh olb vcdik
gsp ilba llnu jjk urbvuma qzypf bkceotg ezxq hyvjngf
tfnegyq rue waeif tfnegyq mvqm
wvgnsk cpd oib wrdfaz kohwgkc kzzig hogkwck gkizz
fecuuyp yfq bvanvxb cjeqwf unw dccr qzh zqu voakj
utoazh bjuq kmhcre izmny mirorsy twnl jyoc
fnnpd dmr ccgu eqgewc zuqivf
kkxiba qdabuen oikaz dnuywmm
aogud adugo uzcglpj lucv dgoua mdsqa mvrg
lymhv sof hvyml mlvhy nit
chu bwxp xpbw ghaix seklnc ola zofnrwt uch
wtt abob vblijtd oabb qjws
uozrpw kgf gxidxm uehdr fta pqakkrq atf fat woaolk
gaee voshd ghlyy emvzlkg cmcgk tuwlsj jwtsul znrta mjieqph glker
qiugxas gkg cbzmoz kahs obzzcm
puz omcokz gjc heuqb
dgndhb wid wdi scwnrjf juaisgo eivaw hgdndb
mgcrd hnqg pkpeb vprxcp
atlcnzp fyp cpkivxi bzj ypf cqpt bysu
pnd jiitmzs csw mxnpck vxutdrs ivipzy cws xiegsy qut
txlk avcvbuu hnq yyriq ajyswd urgiwc
qgiqut gvblizs giqnfrk tty mvoj wpikl giqnfrk bkdpndu xztmxn hsmqxf
llthg zjslki wilj rcyfois bavz hrqxn
ytbw hlkl vip skycogy ejiirhx
ndmtg bthlbw lsoq cvlvo sqol sqlo bppl sdkbls dtpyzrq vgm
psm xpj xjp lqi spm gqirw aglpj
htg fcchvyt xffev szdu lieadft
nbjo qohgzu vofg vvild dbtyi pdolxn plnoao jxze xlpbxj brajzg
urpp jjv lihmvp ivkwdqr sesyp ypbry qok sesyp ivkwdqr was
yinepzv qvnzdtf apv ucxo bdioo juga hjfsyl hmowo avc
dmiv tplae iiuiaxx tpale pyzkc
giwhst mpexd byfyc swuzkc
yydkwp xuu vjya kav ujmcxy qrtp zvlk
lsvdyn tkw qxu omvlc wwmfvov mrgcoov dhpu tfair hupd zbx njzgwtw
zuz rsxc xsrc gdwwf nycsv zzu kcu
unlvzv jerqqgm nozma ykbflj qihqkx
pctffo begf ivrvy ezru mvqt waocq
tubtuk gxkc ikgw bjrird kxjebbh sbjyc yafkd khqajmt aclpmf gqfo yrpf
rdt vrxa fyudo myeosb ursflwk
wbjras edlbwdp ctobtw jbvtvcd xjgoo cmunxm mjtbpi klovx bypmsab unc
xckml uztr htublq vilabvr jdiwus qejxur evfw qqm
tzqq tzqq wkb wkb
dgmg ljzc dgmg mbmco cgze qsap jccvot uors iiq
rwvac woylk dmn teorprx nyuvz hcwwxlj lvej drbjo asjgq
ljen tpfl vixcivr guaf lnje waim jlen
djgaa janhi adudm yzv zkcb xqw fgvrz
kpkjoon ggzx skp rqcsw xgzg zgxg jtf ghc
rtnyxo qixfd nphekk mouzk gny fpzquw qgywx rpr gqydze
gawdlv vrivoof rte iyp gaih sfzplm
csojx wzojode uzy qulr lylmb guvtkwv
ovxj aamms ftxo ebckdqw wqvsdci jwfqxks jafrcrn yyomrot
qnu jqwr ywudxk qpsez rdc kiyfz iiecf dthxjjb bown
typ zxcvjo rip acjhl paaab qhqipg xkguye sbxy pomkvn
ofvaegv hgak oafevgv hkemar rqkha grklnsp msvkkku rekahm bxmjnw
ahoihju sdyn phi uhz lupbx
lavt jef klmq oqyfpf kis nazul ymezxek xpla fxyrfnt
nwnagwy hvpjqfg sgm ungfstr gso owqqxjh
hey hye ipyrt qxmthg jth wpbr hxgmtq cvfkfux qykdzhk movcfnl vxyoc
zsras abnrj fgaczuk ssazr xzf cnxu gns wnqqy dwjh szars
uhb zanlvh lvdotkb xekl kcofo
lhx iccy ibkjw ciykxaj imsx ehamqlz iwzapxc rhaltv
pofit owmpqej vwrobh jvox gdqehss yyxd styu tfkm fiotp
ecz mdpoqsv mdpoqsv yxx rexok hcfll yvury hdhcfu juhkvpt rspnfj hxvgdir
ohed mtigaoe eodh agmiteo
vjvv hfco cppbxtw hawsjxz ovlsq qgs risgwhg auhj
togivgg czrtvw ccz wzvtrc bse lsk
ndc ndc lrfi iyleol nchx jxpv xdcsfmp nnx wtvq pih tgc
hzpf sur zhfp klfmhx lbuidp xiqimnf
qddpdk trfxpip pnsowj hidgvnf prur rsrautp aamykfm fysqjmq xwzjane mbmtxhf oqctt
lfd eops govslp ultbye vrqai hcjkcf snpape
cbok koumkad otpozb pqcs emilpe wpcyvxd bock
spjb xkkak anuvk ejoklh nyerw bsjp zxuq vcwitnd xxtjmjg zfgq xkpf
juo pmiyoh xxk myphio ogfyf dovlmwm moevao qqxidn";

            var rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            var result = 0;
            foreach (var row in rows)
            {
                var words = row.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var sortedLetterWords = words.Select(x => new string(x.OrderBy(y => y).ToArray())).ToList();
                var grouped = sortedLetterWords.GroupBy(x => x);
                if (grouped.Any(x => x.Count() > 1))
                {
                    continue;
                }
                else
                {
                    result++;
                }
            }

        }

        private static void Day3()
        {
            var input = 265149;

            var cases = new Dictionary<(int X, int Y), int>();

            var center = (X: 0, Y: 0);

            var previousLocation = center;
            var previousValue = 1;

            cases.Add(previousLocation, previousValue);

            var XDirection = 1;
            var YDirection = 1;
            var XDirectionCountDown = 1;
            var YDirectionCountDown = 1;
            var XLastCountDown = 1;
            var YLastCountDown = 1;

            var horizontal = true;

            while (previousValue < input)
            {
                (int X, int Y) nextLocation;

                if (horizontal)
                {
                    nextLocation = (previousLocation.X + XDirection, previousLocation.Y);
                    XDirectionCountDown--;
                    if (XDirectionCountDown == 0)
                    {
                        horizontal = false;
                        XDirection *= -1;
                        XDirectionCountDown = XLastCountDown = XLastCountDown + 1;
                    }
                }
                else
                {
                    nextLocation = (previousLocation.X, previousLocation.Y + YDirection);
                    YDirectionCountDown--;
                    if (YDirectionCountDown == 0)
                    {
                        horizontal = true;
                        YDirection *= -1;
                        YDirectionCountDown = YLastCountDown = YLastCountDown + 1;
                    }
                }

                var north = (nextLocation.X, nextLocation.Y + 1);
                var northEast = (nextLocation.X + 1, nextLocation.Y + 1);
                var east = (nextLocation.X + 1, nextLocation.Y);
                var southEast = (nextLocation.X + 1, nextLocation.Y - 1);
                var south = (nextLocation.X, nextLocation.Y - 1);
                var southWest = (nextLocation.X - 1, nextLocation.Y - 1);
                var west = (nextLocation.X - 1, nextLocation.Y);
                var northWest = (nextLocation.X - 1, nextLocation.Y + 1);

                var all = new[] { north, northEast, east, southEast, south, southWest, west, northWest };

                var adjacentCases = from adjacent in all
                                    where cases.ContainsKey(adjacent)
                                    select cases[adjacent];

                var sum = adjacentCases.Sum();


                int nextValue = sum;

                cases.Add(nextLocation, nextValue);
                previousLocation = nextLocation;
                previousValue = nextValue;
            }
        }

        private static void Day2()
        {
            var input = @"790	99	345	1080	32	143	1085	984	553	98	123	97	197	886	125	947
302	463	59	58	55	87	508	54	472	63	469	419	424	331	337	72
899	962	77	1127	62	530	78	880	129	1014	93	148	239	288	357	424
2417	2755	254	3886	5336	3655	5798	3273	5016	178	270	6511	223	5391	1342	2377
68	3002	3307	166	275	1989	1611	364	157	144	3771	1267	3188	3149	156	3454
1088	1261	21	1063	1173	278	1164	207	237	1230	1185	431	232	660	195	1246
49	1100	136	1491	647	1486	112	1278	53	1564	1147	1068	809	1638	138	117
158	3216	1972	2646	3181	785	2937	365	611	1977	1199	2972	201	2432	186	160
244	86	61	38	58	71	243	52	245	264	209	265	308	80	126	129
1317	792	74	111	1721	252	1082	1881	1349	94	891	1458	331	1691	89	1724
3798	202	3140	3468	1486	2073	3872	3190	3481	3760	2876	182	2772	226	3753	188
2272	6876	6759	218	272	4095	4712	6244	4889	2037	234	223	6858	3499	2358	439
792	230	886	824	762	895	99	799	94	110	747	635	91	406	89	157
2074	237	1668	1961	170	2292	2079	1371	1909	221	2039	1022	193	2195	1395	2123
8447	203	1806	6777	278	2850	1232	6369	398	235	212	992	7520	7304	7852	520
3928	107	3406	123	2111	2749	223	125	134	146	3875	1357	508	1534	4002	4417";

            var example = @"5	9	2	8
9	4	7	3
3	8	6	5";
            //input = example;

            var rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            var result = 0;
            foreach (var row in rows)
            {
                var cells = row.Split('\t');
                var parsedCells = cells.Select(x => int.Parse(x)).ToList();
                for (int i = 0; i < parsedCells.Count; i++)
                {
                    var currentCell = parsedCells[i];
                    var otherCells = parsedCells.Where((x, index) => index != i).ToList();
                    var divisionRemainders = otherCells.Select(x =>
                    {
                        var res = Math.DivRem(currentCell, x, out var rem);
                        return ($"{currentCell}/{x}", res, rem);
                    });
                    var evenlyDividedIndex = divisionRemainders
                        .Select((x, index) => (x, index))
                        .Where(x => x.x.rem == 0)
                        .SingleOrDefault();
                    if (evenlyDividedIndex != default)
                    {
                        result += evenlyDividedIndex.x.res;
                    }
                }
            }
        }

        private static void Day1()
        {
            var input = "29917128875332952564321392569634257121244516819997569284938677239676779378822158323549832814412597817651244117851771257438674567254146559419528411463781241159837576747416543451994579655175322397355255587935456185669334559882554936642122347526466965746273596321419312386992922582836979771421518356285534285825212798113159911272923448284681544657616654285632235958355867722479252256292311384799669645293812691169936746744856227797779513997329663235176153745581296191298956836998758194274865327383988992499115472925731787228592624911829221985925935268785757854569131538763133427434848767475989173579655375125972435359317237712667658828722623837448758528395981635746922144957695238318954845799697142491972626942976788997427135797297649149849739186827185775786254552866371729489943881272817466129271912247236569141713377483469323737384967871876982476485658337183881519295728697121462266226452265259877781881868585356333494916519693683238733823362353424927852348119426673294798416314637799636344448941782774113142925315947664869341363354235389597893211532745789957591898692253157726576488811769461354938575527273474399545366389515353657644736458182565245181653996192644851687269744491856672563885457872883368415631469696994757636288575816146927747179133188841148212825453859269643736199836818121559198563122442483528316837885842696283932779475955796132242682934853291737434482287486978566652161245555856779844813283979453489221189332412315117573259531352875384444264457373153263878999332444178577127433891164266387721116357278222665798584824336957648454426665495982221179382794158366894875864761266695773155813823291684611617853255857774422185987921219618596814446229556938354417164971795294741898631698578989231245376826359179266783767935932788845143542293569863998773276365886375624694329228686284863341465994571635379257258559894197638117333711626435669415976255967412994139131385751822134927578932521461677534945328228131973291962134523589491173343648964449149716696761218423314765168285342711137126239639867897341514131244859826663281981251614843274762372382114258543828157464392";
            var example1 = "1212";
            var example2 = "1221";
            var example3 = "123425";
            var example4 = "123123";
            var example5 = "12131415";
            //input = example5;

            int result = 0;
            for (int i = 0; i < input.Length; i++)
            {
                var c = input[i];
                var offset = (input.Length / 2) % (input.Length - 1);
                var nextIndex = (i + offset) % input.Length;
                var next = input[nextIndex];
                if (next == c)
                {
                    result += int.Parse(new string(c, 1));
                }
            }
        }
    }
}
